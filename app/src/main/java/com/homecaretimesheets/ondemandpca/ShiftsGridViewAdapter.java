package com.homecaretimesheets.ondemandpca;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class ShiftsGridViewAdapter extends ArrayAdapter {
    Context context;
    List<Shift> shifts;
    int counter;
    Location location;
    boolean isPca = true;


    public ShiftsGridViewAdapter(Context context, List<Shift> shifts, boolean isPca) {
        super(context, 0);
        this.context = context;
        this.shifts = shifts;
        this.counter = 0;
        this.isPca = isPca;
    }
    public int getCount() { return shifts.size(); }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.show_shifts, parent, false);
        } else {
            row = convertView;
        }
        Shift shift = shifts.get(position);
        TextView dateText = (TextView) row.findViewById(R.id.timeText);
        dateText.setText(shift.timeString);
        TextView distanceText = (TextView) row.findViewById(R.id.distanceText);
        if (isPca) { distanceText.setText(shift.distance); }
        else { distanceText.setVisibility(View.GONE); }
        if (!isPca && !shift.available) {
            TextView availCheck = (TextView) row.findViewById(R.id.availableCheck);
            availCheck.setVisibility(View.VISIBLE);
        }
        else if (!isPca && shift.direct) {
            boolean accepted = false;
            boolean declined = false;
            if (shift.rsvps.size() > 0) {
                RSVP rsvp = shift.rsvps.get(0);
                accepted = rsvp.accepted;
                declined = rsvp.declined;
            }
            TextView availCheck = (TextView) row.findViewById(R.id.availableCheck);
            if (accepted != declined) {
                if (!accepted) {
                    availCheck.setTextColor(context.getResources().getColor(R.color.red));
                    availCheck.setText(R.string.ex);
                }
                availCheck.setVisibility(View.VISIBLE);
            }
        }
        counter++;
        return row;
    }
}