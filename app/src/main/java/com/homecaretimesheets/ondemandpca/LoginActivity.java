package com.homecaretimesheets.ondemandpca;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.core.content.ContextCompat;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.homecaretimesheets.ondemandpca.AuthData.client;
import static com.homecaretimesheets.ondemandpca.AuthData.retestClient;

public class LoginActivity extends AppCompatActivity  {
    public OnDemandGlobal globals;
    public Context context;
    private int sdkVersion;
    private EditText EditUsername;
    private EditText EditPassword;
    private Button Login;
    private Dialog dialog;
    private String username;
    private String passwd;
    public Boolean testing;
    private OnDemandStorage store;
    private Boolean shouldSendToken = true;

    int nextAct = 0;


    private SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy", Locale.US);
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm aa", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        globals = (OnDemandGlobal) getApplicationContext();
        testing = globals.testing;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        context = LoginActivity.this;
        store = new OnDemandStorage(context);
        globals.setLastActive();

        EditUsername = (EditText) findViewById(R.id.EditUsername);
        if (store.hasDataString(ODatum.USERNAME)) {
            username = store.getDataString(ODatum.USERNAME);
            EditUsername.setText(username);
        }
        EditUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                globals.setLastActive();
                username = EditUsername.getText().toString();
                updateAdvanceButton();
            }
        });

        EditPassword = (EditText) findViewById(R.id.EditPassword);
        EditPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                globals.setLastActive();
                passwd = EditPassword.getText().toString();
                updateAdvanceButton();
            }
        });
        EditPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Login.setEnabled(false);
                    globals.setLastActive();
                    loginSubmit();
                }
                return false;
            }
        });
        Login = (Button) findViewById(R.id.login);
        Login.setEnabled(false);
        Login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Login.setEnabled(false);
                globals.setLastActive();
                loginSubmit();
            }
        });

        globals.profileImg = null;
        globals.hasProfileImg = false;
        globals.agencyImage = null;
        globals.hasAgencyImg = false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        globals.currentActivity = 2;
    }


    private void updateAdvanceButton() {
        enableButton(Login, allFieldsEntered());
    }

    private Boolean allFieldsEntered() {
        return !(username == null || passwd == null);
    }

    private void enableButton(Button btn, Boolean enable) {
        if (enable) {
            btn.setTextColor(ContextCompat.getColor(context, R.color.b_color));
            btn.setEnabled(true);
        } else {
            btn.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
            btn.setEnabled(false);
        }
    }

    @SuppressLint("InflateParams")
    private void loginSubmit() {
        globals.setLastActive();
        enableButton(Login, false);
        ApiService apiService = new ApiService();
        ApiService.NewLogin newLogin = apiService.restAdapter.create(ApiService.NewLogin.class);
        ApiService.AuthBody authBody = apiService.new AuthBody();
        authBody.username = username;
        authBody.password = passwd;
        if (testing) {
            authBody.client_id = retestClient;
        }
        else {
            authBody.client_id = client;
        }
        newLogin.authenticate(authBody, logResponse);
        startDialog();
    }

    private void startDialog(){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        //noinspection ConstantConditions
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    Callback<ApiService.AuthResponse> logResponse = new Callback<ApiService.AuthResponse>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Login Failed", sdkVersion).show();
            }
            dialog.dismiss();
            updateAdvanceButton();
        }


        @Override
        public void success(ApiService.AuthResponse authResponse, Response response) {
            store.setDataString(ODatum.USERNAME, username);
            store.setDataString(ODatum.REFRESH_KEY, authResponse.refresh_token);
            globals.setApiToken(authResponse.access_token);
            globals.getData(dataResponse);
        }
    };

    Callback<ApiService.DataResponse> dataResponse = new Callback<ApiService.DataResponse>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Account Info Failed", sdkVersion).show();
            }
            dialog.dismiss();
            globals.resetLogin(context);
            finish();
        }
        @Override
        public void success(ApiService.DataResponse loginResponse, Response response) {
            globals.setUser(loginResponse);
            if (store.hasDataInt(ODatum.USER_ID)) {
               if (store.getDataInt(ODatum.USER_ID) == loginResponse.user.id) {
                   shouldSendToken = false;
               }
               else {
                   store.setDataInt(ODatum.USER_ID, loginResponse.user.id);
               }
            }
            else {
                store.setDataInt(ODatum.USER_ID, loginResponse.user.id);
            }
            if (!globals.hasAgencyImg) {
                Thread t = new Thread(runnable);
                t.start();
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            store.setDataInt(ODatum.USER_ID, loginResponse.user.id);
            if (globals.user.isPca) {
                globals.getPCAData(getPCAResp);
            }
            else {
                globals.getQualificationList(qualificationResp);
            }
        }
    };

    protected Callback<onDemandPCA> getPCAResp = new Callback<onDemandPCA>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "User Data Failed", sdkVersion).show();
            }
            dialog.dismiss();
        }
        @Override
        public void success(onDemandPCA pca, Response response) {
            globals.setPca(pca);
            if (globals.needsProfileImg) {
                Thread t = new Thread(runnable2);
                t.start();
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            globals.getCounties(countyResp);

        }
    };

    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            try{
                String urlString = globals.agencyImageUrl;

                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                if(myBitmap!=null)
                {
                    String fileName = "agency.png";
                    File mypath=new File(getFilesDir(),fileName);
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(mypath);
                        myBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        if (globals.fileExistance(fileName)) {
                            globals.setAgencyImage(mypath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            assert fos != null;
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            catch(Exception e){
                e.printStackTrace();

            }
        }
    };


    Runnable runnable2 = new Runnable() {
        @Override
        public void run() {
            try {
                String pcaImgUrl = globals.amazonUrl + globals.myPCA.profilePhoto;
                pcaImgUrl = pcaImgUrl.replace("\"", "");
                URL url2 = new URL(pcaImgUrl);
                HttpURLConnection connection2 = (HttpURLConnection) url2.openConnection();
                connection2.setDoInput(true);
                connection2.connect();
                InputStream input2 = connection2.getInputStream();

                Bitmap myBitmap2 = BitmapFactory.decodeStream(input2);
                if (myBitmap2 != null) {
                    String fileName = globals.profileFileName;
                    File mypath = new File(getFilesDir(), fileName);
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(mypath);
                        myBitmap2.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        if (globals.fileExistance(fileName)) {
                            globals.profileImg = mypath;
                            globals.hasProfileImg = true;
                        }
                    } catch (Exception e) {
                        globals.hasProfileImg = false;
                        e.printStackTrace();
                    } finally {
                        try {
                            assert fos != null;
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    Callback<List<Qualification>> qualificationResp = new Callback<List<Qualification>>() {
        @Override
        public void success(List<Qualification> quals, Response response) {
            globals.setQualificationList(quals);
            if (shouldSendToken) {
                sendToken();
            }
            else {
                nextActivity();
            }
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Connection Failed", sdkVersion).show();
            }
            dialog.dismiss();
            globals.resetLogin(context);
            finish();
        }
    };

    Callback<List<County>> countyResp = new Callback<List<County>>() {
        @Override
        public void success(List<County> counties, Response response) {
            globals.setCounties(counties);
            globals.getQualificationList(qualificationResp);
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Connection Failed", Toast.LENGTH_SHORT).show();
            }
            globals.resetLogin(context);
            finish();
        }
    };

    public void nextActivity() {
        globals.setLastActive();
        String whichSwitch = getIntent().getStringExtra("push_type");
        String whichShift = getIntent().getStringExtra("request_id");
        if (whichSwitch != null) {
            globals.whichStart = Integer.valueOf(whichSwitch);
        }
        if (whichShift != null) {
            globals.shiftId = Integer.valueOf(whichShift);
        }
        if (!globals.user.isPca) {
            if (whichSwitch == null) {
                dialog.dismiss();
                startActivity(new Intent(context, SelectActivity.class));
                finish();
            }
            else {
                switch (globals.whichStart) {
                    case 2:
                        nextAct = 2;
                        getShift();
                        break;
                    case 4:
                        nextAct = 2;
                        getShift();
                        break;
                    default:
                        dialog.dismiss();
                        startActivity(new Intent(context, SelectActivity.class));
                        finish();
                }
            }
        } else {
            if (globals.hasProfile) {
                if (globals.directRequests.size() > 0) {
                    dialog.dismiss();
                    startActivity(new Intent(context, DirectRequestActivity.class));
                    finish();
                }
                else {
                    switch (globals.whichStart) {
                        case 1:
                            if (globals.shiftId != 0) {
                                nextAct = 1;
                                getShift();
                            } else {
                                dialog.dismiss();
                                startActivity(new Intent(context, FindShiftsActivity.class));
                                finish();
                            }
                            break;
                        case 3:
                            dialog.dismiss();
                            startActivity(new Intent(context, FindShiftsActivity.class));
                            break;

                        default:
                            dialog.dismiss();
                            startActivity(new Intent(context, SelectActivity.class));
                            finish();
                    }
                }
            } else {
                dialog.dismiss();
                startActivity(new Intent(context, ProfileActivity.class));
                finish();
            }
        }
    }


    private void getShift() {
        if (globals.shiftId > 0) {
            ApiService apiService1 = new ApiService(globals.api_key);
            ApiService.GetShift getShift = apiService1.restAdapter.create(ApiService.GetShift.class);
            getShift.getShift(globals.shiftId, setShift);
        }
        else {
            startActivity(new Intent(context, SelectActivity.class));
            finish();
        }
    }

    Callback<Shift> setShift = new Callback<Shift>() {
        @Override
        public void success(Shift currentShift, Response response) {
            Geocoder geoCoder = new Geocoder(context);
            boolean isAvailable = true;
            if (globals.user.isPca) {
                for (RSVP rsvp : currentShift.rsvps) {
                    if (isAvailable) {
                        if (rsvp.accepted) {
                            isAvailable = false;
                        }
                    }
                    if (rsvp.pca.id.equals(globals.myPCA.id)) {
                        currentShift.mine = true;
                    }
                }
            }
            else {
                isAvailable = true;
            }
            if (isAvailable) {
                try {
                    Date start = serverDateFormat.parse(currentShift.startShiftTime);
                    Date end = serverDateFormat.parse(currentShift.endShiftTime);
                    currentShift.timeString = dateFormat.format(start) + " " + timeFormat.format(start) + " - " + timeFormat.format(end);
                    currentShift.distance = "??mi away";
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                if (currentShift.address != null && !currentShift.address.isEmpty()) {
                    try {
                        List<android.location.Address> addressList = geoCoder.getFromLocationName(currentShift.address, 1);
                        if (addressList.size() > 0) {
                            currentShift.lat = addressList.get(0).getLatitude();
                            currentShift.lon = addressList.get(0).getLongitude();
                            currentShift.distance = setDistance(currentShift);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            globals.currentShift = currentShift;
            if (nextAct == 2) {
                startActivity(new Intent(context, RecipShiftDetailActivity.class));
            }
            else {
                startActivity(new Intent(context, ShiftDetailActivity.class));
            }
            finish();
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                }
                else {
                    startActivity(new Intent(context, SelectActivity.class));
                    finish();
                }
            }
        }
    };

    public void sendToken() {
        JsonObject device = new JsonObject();
        device.addProperty("isActive", true);
        device.addProperty("isAndroid", true);
        device.addProperty("isIos", false);
        device.addProperty("user", globals.user.id);
        device.addProperty("deviceToken", globals.deviceToken);
        ApiService.SetDeviceBody deviceBody = globals.apiService.new SetDeviceBody();
        deviceBody.homecare_device = device;
        ApiService.SetDevice setDevice = globals.apiService.restAdapter.create(ApiService.SetDevice.class);
        setDevice.setDevice(deviceBody, deviceResponse);
    }

    Callback<ApiService.SimpleResponse> deviceResponse = new Callback<ApiService.SimpleResponse>() {
        @Override
        public void success(ApiService.SimpleResponse simpleResponse, Response response) {
            nextActivity();
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Connection Failed", sdkVersion).show();
            }
            globals.resetLogin(context);
            finish();
        }
    };

    public String setDistance(Shift shift) {
        String returnString  = "??mi away";
        if (shift.lat != 0 || shift.lon != 0 && globals.currentLocation != null) {
            Location newSpot = new Location("");
            newSpot.setLatitude(shift.lat);
            newSpot.setLongitude(shift.lon);
            Float distance = globals.currentLocation.distanceTo(newSpot);
            double miles = distance * 0.00062137;
            returnString = (Double.valueOf((double) Math.round(miles * 100d) / 100d)).toString() + "mi away";
        }
        return returnString;
    }

}

