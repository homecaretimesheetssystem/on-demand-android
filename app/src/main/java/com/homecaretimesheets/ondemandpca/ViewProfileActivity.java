package com.homecaretimesheets.ondemandpca;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ViewProfileActivity extends AppCompatActivity {
    Context context;
    OnDemandGlobal globals;
    OnDemandStorage store;
    TextView pcaNameView;
    TextView pcaYearsView;
    TextView pcaGenderView;
    TextView pcaAboutMeView;
    TextView pcaNumberView;
    TextView pcaEmailView;
    ListView pcaQualsView;
    ImageView pcaImgView;
    Button pcaSendRequest;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = ViewProfileActivity.this;
        setContentView(R.layout.activity_view_profile);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);

        pcaNameView = (TextView) findViewById(R.id.small_profile_name);
        pcaImgView = (ImageView) findViewById(R.id.small_profile_image);
        pcaGenderView = (TextView) findViewById(R.id.gender_span);
        pcaYearsView = (TextView) findViewById(R.id.year_span);
        pcaQualsView = (ListView) findViewById(R.id.pca_qual_list);
        pcaAboutMeView = (TextView) findViewById(R.id.about_span);
        pcaNumberView = (TextView) findViewById(R.id.phone_span);
        pcaEmailView = (TextView) findViewById(R.id.email_span);
        pcaSendRequest = (Button) findViewById(R.id.send_request);



        List<String> quals = new ArrayList<>();
        for (Qualification qual : globals.myPCA.qualifications) {
            quals.add(qual.qualification.toUpperCase());
        }
        pcaNameView.setText(globals.myPCA.name);
        if (globals.myPCA.gender != null && globals.myPCA.gender != 0) {
            pcaGenderView.setText(OnDemandGlobal.Gender.fromInt(globals.myPCA.gender).name());
        }
        if (globals.myPCA.years_of_experience != null) {
            String yearsXp = String.format(globals.myPCA.years_of_experience.toString(), Locale.US);
            pcaYearsView.setText(yearsXp);
        }
        if (globals.myPCA.about_me != null) {
            pcaAboutMeView.setText(globals.myPCA.about_me);
        }
        if (globals.myPCA.phoneNumber != null) {
            pcaNumberView.setText(globals.myPCA.phoneNumber);
        }
        if (globals.myPCA.email != null) {
            pcaEmailView.setText(globals.myPCA.email);
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, quals);
        pcaQualsView.setAdapter(arrayAdapter);

        pcaSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewShiftActivity.class);
                intent.putExtra("Selected Worker", true);
                startActivity(intent);
                finish();
            }
        });
        if (globals.myPCA.profileImage != null) {
            pcaImgView.setImageBitmap(globals.myPCA.profileImage);
        }
        else if (globals.myPCA.profilePhoto != null){
            Thread t = new Thread(runnable);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (globals.back_to_shift) {
            startActivity(new Intent(context, RecipShiftDetailActivity.class));
        }
        else {
            startActivity(new Intent(context, BrowseWorkersActivity.class));
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recipient, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            resetLogin();
        }
        return super.onOptionsItemSelected(item);
    }

    public void resetLogin() {
        globals.fromRefresh = false;
        store.clearDatum(ODatum.REFRESH_KEY);
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try{
                String pcaImgUrl = globals.amazonUrl + globals.myPCA.profilePhoto;
                pcaImgUrl= pcaImgUrl.replace("\"", "");
                URL url = new URL(pcaImgUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                if(myBitmap!=null)
                {
                    setImage(myBitmap);
                }
            }
            catch(Exception e){
                e.printStackTrace();

            }
        }
    };

    public void setImage(Bitmap myBitmap) {
        pcaImgView.setImageBitmap(myBitmap);
        globals.myPCA.profileImage = myBitmap;
    }
}
