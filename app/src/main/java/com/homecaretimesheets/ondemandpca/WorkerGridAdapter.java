package com.homecaretimesheets.ondemandpca;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class WorkerGridAdapter extends ArrayAdapter {
    Context context;
    List<onDemandPCA> pcas;
    int counter;

    public WorkerGridAdapter(Context context, List<onDemandPCA> workers) {
        super(context, 0);
        this.context = context;
        this.pcas = workers;
        this.counter = 0;
    }
    public int getCount() {
       return pcas.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.show_worker, parent, false);
        }
        else {
            row = convertView;
        }
        final onDemandPCA worker = pcas.get(position);
        TextView workerName = row.findViewById(R.id.workerName);
        workerName.setText(worker.name);

        counter++;
        return row;
    }
}
