package com.homecaretimesheets.ondemandpca;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
//import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class PermissionService {
    public boolean getLocationPermission(Context context) {
        boolean retBool = false;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, R.string.app_name + " requires location services to continue", Toast.LENGTH_LONG).show();
        }
        else {
            retBool = true;
        }
        return retBool;
    }

    public boolean getStoragePermission(Context context) {
        boolean retBool = false;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, R.string.app_name + " requires access to Storage to continue", Toast.LENGTH_LONG).show();
        }
        else {
            retBool = true;
        }
        return retBool;
    }

    public boolean getNetworkPermission(Context context) {
        boolean retBool = false;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, R.string.app_name + " requires access to Network State to continue", Toast.LENGTH_LONG).show();
        }
        else {
            retBool = true;
        }
        return retBool;
    }

    public boolean getInternetPermission(Context context) {
        boolean retBool = false;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, R.string.app_name + " requires access to Internet to continue", Toast.LENGTH_LONG).show();
        }
        else {
            retBool = true;
        }
        return retBool;
    }


}
