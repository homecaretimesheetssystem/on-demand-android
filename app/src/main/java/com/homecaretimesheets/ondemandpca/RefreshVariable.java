package com.homecaretimesheets.ondemandpca;

public class RefreshVariable {
    private boolean refresh = false;
    private ChangeListener listener;
    public boolean needsRefresh() {
        return refresh;
    }

    public void setRefresh(boolean bool) {
        this.refresh = bool;
        if (bool) {
            if (listener != null) listener.onChange();
        }
        this.refresh = false;
    }

    public ChangeListener getListener() {
        return listener;
    }

    public void setListener(ChangeListener listener) {
        this.listener = listener;
    }

    public interface ChangeListener {
        void onChange();
    }
}
