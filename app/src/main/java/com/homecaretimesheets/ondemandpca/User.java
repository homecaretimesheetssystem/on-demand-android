package com.homecaretimesheets.ondemandpca;

import java.util.List;

public class User {
    public int id = 0;
    String userType;
    int userSet;
    String email;
    Agency agency;
    Recipient recipient;
    List<Recipient> recipients;
    boolean isPca = false;
    PCA pca;
    boolean has_profile;

    User setUser(UserData userData) {
        this.id = userData.id;
        this.userType = userData.userType.toUpperCase();
        this.email = userData.email;
        if (this.userType.equals("PCA")) {
            this.userSet = 1;
            isPca = true;
        }
        else {
            this.userSet = 2;
        }
        return this;
    }

    void setAgency(Agency agency) {
        this.agency = agency;
    }


    public void setRecips(List<Recipient> recips) {
        this.recipients = recips;
    }


    public void setPCA(PCA pca) {
        this.pca = pca;
    }
}