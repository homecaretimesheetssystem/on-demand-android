package com.homecaretimesheets.ondemandpca;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.sql.Array;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public class ApiService {
    OnDemandGlobal globals = new OnDemandGlobal();
    Boolean DEBUG = globals.testing;
    private final static String API_PATH = "https://staging.homecaretimesheetsapp.com/api/v1/";
    private final static String TEST_API = "https://staging.homecaretimesheetsapp.com/api/v1/";


    public RestAdapter restAdapter;

    ApiService() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String logPath = API_PATH;
        retrofit.RestAdapter.LogLevel logLevel = RestAdapter.LogLevel.NONE;
        if (DEBUG) {
            logPath = TEST_API;
            logLevel = RestAdapter.LogLevel.FULL;
        }
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(logPath)
                .setLogLevel(logLevel)
                .setConverter(new GsonConverter(gson))
                .build();
    }

    ApiService(final String api_token) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "Bearer " + api_token);
            }
        };
        String endPath = API_PATH;
        retrofit.RestAdapter.LogLevel logLevel = RestAdapter.LogLevel.NONE;
        if (DEBUG) {
            endPath = TEST_API;
            logLevel = RestAdapter.LogLevel.FULL;
        }
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPath)
                .setLogLevel(logLevel)
                .setRequestInterceptor(requestInterceptor)
                .build();
    }



    interface NewLogin {
        @POST("/login")
        void authenticate(@Body AuthBody authBody, Callback<AuthResponse> response);
        void refreshToken(@Body RefreshBody refreshBody, Callback<AuthResponse> response);
    }


    class AuthBody {
        String username;
        String password;
        String client_id;
    }

    class RefreshBody {
        String refresh_token;
        String client_id;
    }

    class AuthResponse {
        String access_token;
        String refresh_token;
        String error;
        String error_description;
    }


    interface RefreshService {
        @GET("/token")
        void authenticate(@Query("client_id") String client_id,
                          @Query("client_secret") String client_secret,
                          @Query("grant_type") String grant_type,
                          @Query("refresh_token") String refresh_token,
                          Callback<RefreshResponse> response);
    }

    class RefreshResponse {
        String access_token;
        String refresh_token;
    }

    interface DataService {
        @GET("/data")
        void getData(Callback<DataResponse> response);
    }

    class DataResponse {
        public UserData user;
        public PCA pca;
        public Recipient recipient;
        Agency agency;
        String amazonUrl;
    }

    @SuppressWarnings("unused")
    class SimpleResponse {
        boolean result;
    }

    interface QualificationsList {
        @GET("/qualifications")
        void getQualifications(Callback<List<Qualification>> response);
    }

    interface GetCounties {
        @GET("/counties")
        void getCounties(Callback<List<County>> response);
    }

    interface GetAvailShifts {
        @GET("/pcas/{pca_id}/available_requests")
        void getShifts(@Path("pca_id") int pca_id, Callback<List<Shift>> response);
    }

    interface GetShift {
        @GET("/requests/{request_id}")
        void getShift(@Path("request_id") int request_id, Callback<Shift> response);
    }

    @SuppressWarnings("unused")
    private class GetPostResponse {
        public boolean result;
    }

    interface SendRSVP {
        @POST("/rsvps")
        void sendAcceptance(@Body JsonObject rsvp, Callback<SimpleResponse> response);
    }

    interface CancelRSVP {
        @DELETE("/rsvp/{rsvp_id}")
        void sendAcceptance(@Path("rsvp_id") int rsvp_id, Callback<SimpleResponse> response);
    }


    interface GetPCAShifts {
        @GET("/pcas/{pca_id}/accepted_requests")
        void getShifts(@Path("pca_id") int pca_id, Callback<List<Shift>> response);
    }

    interface GetRecipShifts {
        @GET("/recipient/{recip_id}/requests")
        void getShifts(@Path("recip_id") int recip_id, Callback<List<Shift>> response);
    }

    interface GetPCA {
        @GET("/pcas/{pca_id}")
        void getData(@Path("pca_id") int pca_id, Callback<onDemandPCA> response);
    }

    interface SetPCAProfile {
        @PATCH("/pcas/{pca_id}")
        void setProfile(@Path("pca_id") int pca_id, @Body SetPCAProfileBody body, Callback<SimpleResponse> response);
    }

    interface SendProfileImage {
        @Multipart
        @POST("/pcas/{pca_id}/image")
        void setImage(@Path("pca_id") int pca_id, @Part(value = "profilePhoto") TypedFile file, Callback<SimpleResponse> response);
    }

    class SetPCAProfileBody {
        JsonObject homecare_profile;
    }


    interface SetDevice {
        @POST("/devices")
        void setDevice(@Body SetDeviceBody device, Callback<SimpleResponse> response);
    }

    class SetDeviceBody {
        JsonObject homecare_device;
    }

    interface SendRequest {
        @POST("/requests")
        void sendRequest(@Body ShiftRequestBody body, Callback<Void> response);
    }

    class ShiftRequestBody {
        JsonObject homecare_request;
    }

    @SuppressWarnings("unused")
    class ShiftRequestResp {
    }

    interface CancelRequest {
        @DELETE("/requests/{request_id}")
        void cancelRequest(@Path("request_id") int request_id, Callback<SimpleResponse> response);
    }

    @SuppressWarnings("unused")
    class ContactResponse {
        boolean result;
        public List<onDemandPCA> pcas;
    }

    interface GetPcaContacts {
        @GET("/pca_contacts")
        void getContacts(Callback<ContactResponse> response);
    }
}
