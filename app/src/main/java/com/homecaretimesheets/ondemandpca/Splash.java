package com.homecaretimesheets.ondemandpca;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.iid.FirebaseInstanceId;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Splash extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    Context context;
    OnDemandStorage store;
    OnDemandGlobal globals;
    Integer sdkVersion;

    private int INET_PERMISSION = OnDemandGlobal.INTERNET_REQUEST;
    private int NET_PERMISSION = OnDemandGlobal.NETWORK_REQUEST;
    public PermissionService permissionService;
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;
    int connectRequest = OnDemandGlobal.CONNECT_REQUEST;
    boolean netChecked = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        context = Splash.this;
        store = new OnDemandStorage(context);
        globals = (OnDemandGlobal) getApplicationContext();
        permissionService = new PermissionService();
        checkNet();
        globals.deviceToken = FirebaseInstanceId.getInstance().getToken();
        setContentView(R.layout.splash);
        checkLocationPermission();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        buildGoogleApiClient();
        createLocationRequest();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    public void setPage() {
        int secondsDelayed = 3;
        new Handler().postDelayed(new Runnable() {
                public void run() {
                     nextActivity();
                }
            }, secondsDelayed * 1000);
    }

    public void resetLogin() {
        startActivity(new Intent(Splash.this, LoginActivity.class));
        finish();
    }

    public void setPermissions() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(R.string.permission_title)
                .setMessage(R.string.permission_text)
                .setCancelable(false)
                .setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
                        dialog.cancel();
                        checkLocationPermission();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        netChecked = checkNet();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        globals.currentActivity = 1;
    }

    public void getNetPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, NET_PERMISSION);
    }

    public void getINetPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INET_PERMISSION);
    }

    public void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    connectRequest);
        }
        else {
            setPage();
        }
    }

    public boolean checkNet() {
        boolean retCheck = false;
        if (permissionService.getNetworkPermission(context)) {
            Utils.verifyNetworkConnection(context, sdkVersion);
            retCheck = Utils.isNetworkConnected(context);
            if (!retCheck) {
                Toast.makeText(context, "No network connection", Toast.LENGTH_SHORT).show();
            }
            if (!permissionService.getInternetPermission(context)) {
                getINetPermission();
            }
        }
        else {
            getNetPermission();
        }
        return retCheck;
    }

    public void nextActivity() {
        globals.setLastActive();
        String whichSwitch = getIntent().getStringExtra("push_type");
        String whichShift = getIntent().getStringExtra("request_id");
        if (whichSwitch != null) {
            globals.whichStart = Integer.valueOf(whichSwitch);
        }
        if (whichShift != null) {
            globals.shiftId = Integer.valueOf(whichShift);
        }
        startActivity(new Intent(context, LoginActivity.class));
        finish();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == INET_PERMISSION) {
            if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setPermissions();
            }
        }
        else if (requestCode == NET_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                netChecked = checkNet();
                setPermissions();
            }

        }
        else if (requestCode == connectRequest) {
            if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
                createLocationRequest();
                checkLocationPermission();
            }
            else {
                setPermissions();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnectionSuspended(int arg0) {}

    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@Nullable ConnectionResult arg0) {
        Toast.makeText(context, "Location Service Failed", Toast.LENGTH_SHORT).show();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        int UPDATE_INTERVAL = 10000; // 10 sec
        int FASTEST_INTERVAL = 5000; // 5 sec
        int DISPLACEMENT = 100; // 100 meters
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, connectRequest);
            return;
        }
        LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
        globals.currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        globals.currentLocation = location;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	connectRequest);
            return;
        }
        createLocationRequest();
        startLocationUpdates();
    }
}
