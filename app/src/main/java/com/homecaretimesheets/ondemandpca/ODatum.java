package com.homecaretimesheets.ondemandpca;

public enum ODatum {
    USERNAME,
    USER_ID,
    API_KEY,
    REFRESH_KEY,
    USER_ADDRESS
}
