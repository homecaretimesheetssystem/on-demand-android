package com.homecaretimesheets.ondemandpca;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.core.content.ContextCompat;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class RecipShiftDetailActivity extends AppCompatActivity {

    Context context;
    private ApiService apiService;
    OnDemandGlobal globals;
    OnDemandStorage store;
    Shift shift;
    Date timeNow;
    Date startTime;
    Button availButton;
    LinearLayout infoHolder;
    TextView timeView;
    TextView serviceView;
    TextView addressView;
    TextView additionRequirements;
    Button viewProfile;
    Button shiftCancel;
    TextView pcaName;
    ImageView pcaImg;

    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = RecipShiftDetailActivity.this;
        setContentView(R.layout.activity_recip_detail);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        apiService = globals.apiService;
        shift = globals.currentShift;
        availButton = (Button) findViewById(R.id.is_available);
        shiftCancel = (Button) findViewById(R.id.recipCancelShift);
        shiftCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelShift();
            }
        });
        timeView = (TextView) findViewById(R.id.timeSpan);
        timeView.setText(shift.timeString);
        addressView = (TextView) findViewById(R.id.addressSpan);
        addressView.setText(shift.address);
        additionRequirements = (TextView) findViewById(R.id.addedInfo);
        additionRequirements.setText(shift.additional_requirements);
        serviceView = (TextView) findViewById(R.id.serviceSpan);
        infoHolder = (LinearLayout) findViewById(R.id.pca_info_holder);
        viewProfile = (Button) findViewById(R.id.view_profile);
        viewProfile.setEnabled(false);
        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProfile();
            }
        });
        timeNow = new Date();
        startTime = new Date();
        pcaName = (TextView) findViewById(R.id.small_profile_name);
        pcaImg = (ImageView) findViewById(R.id.small_profile_image);
        try {
            startTime = serverDateFormat.parse(shift.startShiftTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        globals.freshVariable.setListener(new RefreshVariable.ChangeListener() {
            @Override
            public void onChange() {
                if (globals.whichStart == 2 || globals.whichStart == 4) {
                    if (shift.id == globals.shiftId) {
                        getShift();
                    }
                }
            }
        });
        globals.back_to_shift = true;
    }

    public void checkAccept() {
        boolean accepted = false;
        for (RSVP rsvp : shift.rsvps) {
            if (!accepted) {
                if (rsvp.accepted) {
                    accepted = true;
                    globals.current_pca = rsvp.pca;
                }
            }
        }
        if (accepted) {
            availButton.setText(R.string.accepted);
            availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
            availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.green_border_invert));
            pcaName.setText(globals.upCaseWords(globals.current_pca.getName()));
            if (globals.current_pca.profileImage != null) {
                pcaImg.setImageBitmap(globals.current_pca.profileImage);
            }
            else if (globals.current_pca.profilePhoto != null) {
                Thread t = new Thread(runnable);
                t.start();
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(context, "No Image Available", Toast.LENGTH_SHORT).show();
            }
            viewProfile.setEnabled(true);
            infoHolder.setVisibility(View.VISIBLE);
        }
        else if (shift.direct) {
            if (shift.rsvps.size() > 0) {
                availButton.setText(R.string.declined);
                availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
                availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.red_border_invert));
                availButton.setEnabled(false);
                RSVP rsvp = shift.rsvps.get(0);
                globals.current_pca = rsvp.pca;
            }
            else {
                availButton.setText(R.string.waiting);
                availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
                availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.dark_gray_border_invert));
                availButton.setEnabled(false);
                globals.current_pca = shift.pca;
            }
            if (globals.current_pca != null) {
                String pcaname = globals.upCaseWords(globals.current_pca.getName());
                pcaName.setText(pcaname);
                if (globals.current_pca.profileImage != null) {
                    pcaImg.setImageBitmap(globals.current_pca.profileImage);
                } else if (globals.current_pca.profilePhoto != null) {
                    Thread t = new Thread(runnable);
                    t.start();
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "No Image Available", Toast.LENGTH_SHORT).show();
                }
                viewProfile.setEnabled(true);
                infoHolder.setVisibility(View.VISIBLE);
            }
        }
        else {
            availButton.setText(R.string.available);
            availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
            availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.dark_gray_border_invert));
            availButton.setEnabled(false);
            infoHolder.setVisibility(View.GONE);
            viewProfile.setEnabled(false);
        }
    }

    private void getShift() {
        ApiService.GetShift getShift = apiService.restAdapter.create(ApiService.GetShift.class);
        getShift.getShift(shift.id, setShift);
    }


    Callback<Shift> setShift = new Callback<Shift>() {
        @Override
        public void success(Shift currentShift, Response response) {
            if (currentShift.id == shift.id) {
                shift = currentShift;
                checkAccept();
            }
            else {
                Toast.makeText(context, "Shift has been removed", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recipient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, MyShiftsActivity.class));
        finish();
    }


    @Override
    public void onStart() {
        super.onStart();
        globals.currentActivity = 6;
    }

    @Override
    public void onResume() {
        super.onResume();
        getShift();
    }
    private void goToProfile() {
        globals.myPCA = globals.current_pca;
        startActivity(new Intent(context,ViewProfileActivity.class));
        finish();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try{
                String pcaImgUrl = globals.amazonUrl + globals.current_pca.profilePhoto;
                pcaImgUrl= pcaImgUrl.replace("\"", "");
                URL url = new URL(pcaImgUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                if(myBitmap!=null)
                {
                    setImage(myBitmap);
                }
            }
            catch(Exception e){
                e.printStackTrace();

            }
        }
    };

    public void setImage(Bitmap myBitmap) {
        pcaImg.setImageBitmap(myBitmap);
        globals.current_pca.profileImage = myBitmap;
    }

    public void cancelShift() {
        ApiService.CancelRequest cancelRequest = apiService.restAdapter.create(ApiService.CancelRequest.class);
        cancelRequest.cancelRequest(shift.id, cancelResponse);
    }

    Callback<ApiService.SimpleResponse> cancelResponse = new Callback<ApiService.SimpleResponse>() {

        @Override
        public void success(ApiService.SimpleResponse simpleResponse, Response response) {
            Toast.makeText(context, "Shift Cancelled", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Cancel Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
