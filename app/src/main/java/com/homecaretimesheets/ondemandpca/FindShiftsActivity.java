package com.homecaretimesheets.ondemandpca;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FindShiftsActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    Context context;
    private int sdkVersion;
    private ApiService apiService;
    protected ApiService.GetAvailShifts getShifts;
    private Dialog dialog;
    OnDemandGlobal globals;
    TextView noShiftsText;
    TextView unavailableText;

    Switch availSwitch;
    GridView shiftView;
    OnDemandStorage store;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy", Locale.US);
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm aa", Locale.US);


    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;
    int connectRequest = OnDemandGlobal.CONNECT_REQUEST;
    int coarseRequest = OnDemandGlobal.UPDATE_REQUEST;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = FindShiftsActivity.this;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_find_shifts);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        apiService = globals.apiService;
        globals.back_to_find = true;
        if (!globals.hasProfile) {
            startActivity(new Intent(context, ProfileActivity.class));
            finish();
        }
        else {
            shiftView = (GridView) findViewById(R.id.grid_layout);
            availSwitch = (Switch) findViewById(R.id.availSwitch);
            availSwitch.setChecked(globals.myPCA.available);
            availSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    globals.myPCA.available = isChecked;
                    setAvailability(isChecked);
                }
            });
            setProfileImage();
            unavailableText = (TextView) findViewById(R.id.unavailable_text);
            if (!globals.myPCA.available) {
                unavailableText.setVisibility(View.VISIBLE);
            }
            else {
                unavailableText.setVisibility(View.GONE);
            }
        }
        noShiftsText = (TextView) findViewById(R.id.no_shifts);
        checkLocationPermission();
        globals.freshVariable.setListener(new RefreshVariable.ChangeListener() {
            @Override
            public void onChange() {
                handleShifts(globals.myPCA.available);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        globals.currentActivity = 4;
    }

    @Override
    public void onResume() {
        super.onResume();
        handleShifts(globals.myPCA.available);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        handleShifts(globals.myPCA.available);
        buildGoogleApiClient();
        createLocationRequest();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }


    public void handleShifts(boolean unlock){
        if (dialog != null) {
            dialog.dismiss();
        }
        if (unlock) {
            setLoadingDialog();
            getShiftData();
            noShiftsText.setVisibility(View.GONE);
            unavailableText.setVisibility(View.GONE);
        }
        else {
            List<Shift> newList = new ArrayList<>();
            ShiftsGridViewAdapter adapter = new ShiftsGridViewAdapter(context, newList, true);
            shiftView.setAdapter(adapter);
            noShiftsText.setVisibility(View.GONE);
            unavailableText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (globals.user.isPca) {
            getMenuInflater().inflate(R.menu.pca, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.recipient, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.prof_bar) {
            goToProfile();
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToProfile() {
        startActivity(new Intent(context,ProfileActivity.class));
        finish();
    }

    private void setLoadingDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        //noinspection ConstantConditions
        dialog.getWindow().setBackgroundDrawableResource(R.color.clear);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void setProfileImage() {
        if (globals.profileImg != null){
            ImageView profileView = (ImageView) findViewById(R.id.profileImage);
            profileView.setImageURI(Uri.fromFile(globals.profileImg));
        }
    }

    private void getShiftData() {
        getShifts = apiService.restAdapter.create(ApiService.GetAvailShifts.class);
        getShifts.getShifts(globals.myPCA.id, shiftResp);
    }


    protected Callback<List<Shift>> shiftResp = new Callback<List<Shift>>() {

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
                dialog.dismiss();
                globals.resetLogin(context);
                finish();
            }
            else {
                dialog.dismiss();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Shift Request Failed", sdkVersion).show();
                }
            }
        }
        @Override
        public void success(List<Shift> shiftList, Response response) {
            List<Shift> shifts = convertAddress(shiftList);
            setShifts(shifts);
            if (shifts.size() == 0) {
                noShifts();
            }
            dialog.dismiss();
        }
    };

    public List<Shift> convertAddress(List<Shift> shifts) {
        Geocoder geoCoder = new Geocoder(context);
        List<Shift> newList = new ArrayList<>();
        for (Shift shift : shifts) {
            boolean isAvailable = true;
            for (RSVP rsvp : shift.rsvps) {
                if (isAvailable) {
                    if (rsvp.accepted) {
                        isAvailable = false;
                    }
                }
                if (rsvp.pca.id.equals(globals.myPCA.id)) {
                    shift.mine = true;
                }
            }
            if (isAvailable) {
                boolean addMe = true;
                try {
                    Date start = serverDateFormat.parse(shift.startShiftTime);
                    if (start.getTime() < globals.getStartOfDay().getTime()) {
                        addMe = false;
                    }
                    else {
                        Date end = serverDateFormat.parse(shift.endShiftTime);
                        shift.timeString = dateFormat.format(start) + " " + timeFormat.format(start) + " - " + timeFormat.format(end);
                        shift.distance = "??mi away";
                    }
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                if (shift.address != null && !shift.address.isEmpty() && addMe) {
                    try {
                        List<android.location.Address> addressList = geoCoder.getFromLocationName(shift.address, 1);
                        if (addressList.size() > 0) {
                            shift.lat = addressList.get(0).getLatitude();
                            shift.lon = addressList.get(0).getLongitude();
                            shift.distance = setDistance(shift);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    newList.add(shift);
                }
            }
        }
        return newList;
    }

    public String setDistance(Shift shift) {
        String returnString  = "??mi away";
        if (shift.lat != 0 || shift.lon != 0 && globals.currentLocation != null) {
            Location newSpot = new Location("");
            newSpot.setLatitude(shift.lat);
            newSpot.setLongitude(shift.lon);
            Float distance = globals.currentLocation.distanceTo(newSpot);
            double miles = distance * 0.00062137;
            returnString = (Double.valueOf((double) Math.round(miles * 100d) / 100d)).toString() + "mi away";
        }
        return returnString;
    }

    public void setShifts(final List<Shift> shifts) {
        globals.shifts = shifts;
        ShiftsGridViewAdapter adapter = new ShiftsGridViewAdapter(context, shifts, true);
        shiftView.setAdapter(adapter);
        shiftView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getDetail(shifts.get(position));
            }
        });
    }

    public void getDetail(Shift shift) {
        globals.currentShift = shift;
        startActivity(new Intent(context, ShiftDetailActivity.class));
        finish();
    }

    public void noShifts() {
        noShiftsText.setVisibility(View.VISIBLE);
    }

    public void setAvailability(boolean available) {
        ApiService.SetPCAProfile setAvailable = apiService.restAdapter.create(ApiService.SetPCAProfile.class);
        JsonObject profile = new JsonObject();
        profile.addProperty("available", available);
        ApiService.SetPCAProfileBody profileBody = apiService.new SetPCAProfileBody();
        profileBody.homecare_profile = profile;
        setAvailable.setProfile(globals.myPCA.id, profileBody, setAvailableResp);
    }

    Callback<ApiService.SimpleResponse> setAvailableResp = new Callback<ApiService.SimpleResponse>() {
        @Override
        public void success(ApiService.SimpleResponse setPCAProfileResp, Response response) {
            handleShifts(globals.myPCA.available);

        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, SelectActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    connectRequest);
        }
        else if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    coarseRequest);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == connectRequest) {
            if (grantResults.length > 0  || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
                createLocationRequest();
                checkLocationPermission();
            }
            else {
                globals.resetLogin(context);
                finish();
            }
        }
        if (requestCode == coarseRequest) {
            if (grantResults.length > 0  || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission();
            }
            else {
                globals.resetLogin(context);
                finish();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnectionSuspended(int arg0) {}

    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@Nullable ConnectionResult arg0) {
        Toast.makeText(context, "Location Service Failed", sdkVersion).show();
        mGoogleApiClient.connect();
    }


    protected void createLocationRequest() {
        int UPDATE_INTERVAL = 10000; // 10 sec
        int FATEST_INTERVAL = 5000; // 5 sec
        int DISPLACEMENT = 100; // 100 meters
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, connectRequest);
            return;
        }
        LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
        globals.currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        globals.currentLocation = location;
        handleShifts(true);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	connectRequest);
            return;
        }
        createLocationRequest();
        startLocationUpdates();
    }
}
