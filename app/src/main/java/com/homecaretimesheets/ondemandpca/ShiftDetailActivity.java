package com.homecaretimesheets.ondemandpca;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.core.content.ContextCompat;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ShiftDetailActivity extends AppCompatActivity {
    Context context;
    private ApiService apiService;
    OnDemandGlobal globals;
    OnDemandStorage store;
    Shift shift;
    Button acceptButton;
    TextView timeView;
    TextView serviceView;
    TextView distanceView;
    Date timeNow;
    Date startTime;
    Button availButton;
    Button shiftCancel;
    Button openHCTS;
    LinearLayout acceptHolder;
    LinearLayout detailHolder;
    TextView addressView;
    TextView recipName;
    TextView addedInfo;
    Button getDirections;
    boolean acceptance = true;
    int my_rsvp_id;

    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = ShiftDetailActivity.this;
        setContentView(R.layout.activity_shift_detail);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        apiService = globals.apiService;
        shift = globals.currentShift;
        availButton = (Button) findViewById(R.id.is_available);
        timeView = (TextView) findViewById(R.id.timeSpan);
        timeView.setText(shift.timeString);
        serviceView = (TextView) findViewById(R.id.serviceSpan);
        distanceView = (TextView) findViewById(R.id.distanceSpan);
        distanceView.setText(shift.distance);
        detailHolder = (LinearLayout) findViewById(R.id.info_holder);
        addressView = (TextView) findViewById(R.id.address);
        recipName = (TextView) findViewById(R.id.recipName);
        shiftCancel = (Button) findViewById(R.id.cancelShiftPca);
        addedInfo = findViewById(R.id.additional_information);
        addedInfo.setText(shift.additional_requirements);
        openHCTS = (Button) findViewById(R.id.get_time_entry);
        getDirections = (Button) findViewById(R.id.get_directions);
        acceptHolder = (LinearLayout) findViewById(R.id.accept_holder);
        acceptButton = (Button) findViewById(R.id.accept_shift);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptButton.setEnabled(false);
                sendRSVP();
            }
        });
        timeNow = new Date();
        startTime = new Date();
        checkAccept();
        try {
            startTime = serverDateFormat.parse(shift.startShiftTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        globals.freshVariable.setListener(new RefreshVariable.ChangeListener() {
            @Override
            public void onChange() {
                if (globals.whichStart == 3) {
                        getShift();
                }
            }
        });
    }


    public void checkAccept() {
        if (shift.rsvps != null && shift.rsvps.size() > 0) {
            for (RSVP rsvp : shift.rsvps) {
                if (globals.myPCA.id.equals(rsvp.pca.id)) {
                    my_rsvp_id = rsvp.id;
                }
                if (rsvp.accepted && globals.myPCA.id.equals(rsvp.pca.id)) {
                    acceptHolder.setVisibility(View.GONE);
                    availButton.setText(R.string.accepted);
                    availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
                    availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.green_border_invert));
                    addressView.setText(shift.address);
                    String recipient_name = globals.upCaseWords(shift.recipient.first_name) + " " + globals.upCaseWords(shift.recipient.last_name);
                    recipName.setText(recipient_name);
                    getDirections.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                    Uri.parse("google.navigation:q=" + shift.address));
                            startActivity(intent);
                        }
                    });
                    shiftCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cancelShift();
                        }
                    });
                    openHCTS.setEnabled(true);
                    openHCTS.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startHCTS();
                        }
                    });
                    detailHolder.setVisibility(View.VISIBLE);
                } else if (rsvp.declined) {
                    availButton.setText(R.string.declined);
                    availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
                    availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.red_border_invert));
                    detailHolder.setVisibility(View.GONE);
                    acceptHolder.setVisibility(View.VISIBLE);
                    acceptButton.setEnabled(true);
                } else {
                    Toast.makeText(context, R.string.taken, Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }
        }
        else {
            availButton.setText(R.string.available);
            availButton.setTextColor(ContextCompat.getColor(context, R.color.white));
            availButton.setBackground(ContextCompat.getDrawable(context, R.drawable.dark_gray_border_invert));
            acceptHolder.setVisibility(View.VISIBLE);
            acceptButton.setEnabled(true);
            detailHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (globals.user.isPca) {
            getMenuInflater().inflate(R.menu.pca, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.recipient, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.prof_bar) {
            goToProfile();
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        globals.currentActivity = 5;
    }

    @Override
    public void onResume() {
        super.onResume();
        getShift();
    }
    private void goToProfile() {
        startActivity(new Intent(context,ProfileActivity.class));
        finish();
    }

    private void sendRSVP() {
        acceptance = true;
        ApiService.SendRSVP sendRSVP = apiService.restAdapter.create(ApiService.SendRSVP.class);
        JsonObject rsvp = new JsonObject();
        JsonObject in_rsvp = new JsonObject();
        in_rsvp.addProperty("pca", globals.myPCA.id);
        in_rsvp.addProperty("request", shift.id);
        in_rsvp.addProperty("accepted", true);
        rsvp.add("homecare_rsvp", in_rsvp);
        sendRSVP.sendAcceptance(rsvp, sendRSVPResp);
    }

    private void cancelShift() {
        acceptance = false;
        detailHolder.setVisibility(View.GONE);
        ApiService.CancelRSVP sendRSVP = apiService.restAdapter.create(ApiService.CancelRSVP.class);
        sendRSVP.sendAcceptance(my_rsvp_id, sendRSVPResp);
    }

    Callback<ApiService.SimpleResponse> sendRSVPResp = new Callback<ApiService.SimpleResponse>() {
        @Override
        public void success(ApiService.SimpleResponse sendAcceptResp, Response response) {
            if (acceptance) {
                Toast.makeText(context, "Acceptance Sent", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Cancellation Sent", Toast.LENGTH_SHORT).show();
            }
            getShift();
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                }
                else {
                    Toast.makeText(context, "Accept Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

        private void getShift() {
            ApiService.GetShift getShift = apiService.restAdapter.create(ApiService.GetShift.class);
            getShift.getShift(shift.id, setShift);
        }

        Callback<Shift> setShift = new Callback<Shift>() {
            @Override
            public void success(Shift currentShift, Response response) {
                shift = currentShift;
                if (globals.user.isPca && checkShift()) {
                    acceptHolder.setVisibility(View.GONE);
                    checkAccept();
                }
            }

            @Override
            public void failure(RetrofitError code) {
                if (code.isNetworkError()) {
                    Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                    globals.resetLogin(context);
                }
                else {
                    Response r = code.getResponse();
                    if (r != null && r.getStatus() == 401) {
                        globals.resetLogin(context);
                    }
                    else {
                        onBackPressed();
                    }
                }
            }
        };

    public boolean checkShift() {
        boolean returnValue = false;
        if (shift.rsvps.isEmpty()) {
            returnValue = true;
        }
        for (RSVP rsvp : shift.rsvps) {
            if (!shift.mine) {
                if (rsvp.pca.id.equals(globals.myPCA.id)) {
                    shift.mine = true;
                }
            }
        }
        return shift.mine || returnValue;
    }

    @Override
    public void onBackPressed() {
        if (globals.back_to_find) {
            startActivity(new Intent(context, FindShiftsActivity.class));
        }
        else {
            startActivity(new Intent(context, MyShiftsActivity.class));
        }
        finish();
    }


    public void startHCTS() {
        final String packageName = "com.homecaretimesheet.apidemo";
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams")
            View dialogLayout = inflation.inflate(R.layout.hcts_dialog, null);
            Button installHCTS = (Button) dialogLayout.findViewById(R.id.hcts_button);
            alertDialogBuilder.setTitle(R.string.install_hcts);
            alertDialogBuilder.setView(dialogLayout);
            final AlertDialog alertDialog = alertDialogBuilder.create();

            installHCTS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + packageName));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
        else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}