package com.homecaretimesheets.ondemandpca;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DirectRequestActivity extends AppCompatActivity {
    Shift shift;
    public Context context;
    private ApiService apiService;
    OnDemandGlobal globals;
    TextView recipientNameTextView;
    TextView startTimeTextView;
    TextView endTimeTextView;
    TextView addressTextView1;
    TextView addressTextView2;
    TextView additionalRequirementsTextView;
    Button acceptButton;
    Button declineButton;
    Boolean accept = false;
    private Dialog dialog;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy", Locale.US);
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm aa", Locale.US);


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = DirectRequestActivity.this;
        setContentView(R.layout.activity_handle_direct_request);
        globals = (OnDemandGlobal) getApplicationContext();
        apiService = globals.apiService;
        shift = globals.directRequests.get(0);
        recipientNameTextView = findViewById(R.id.recipientNameTextView);
        startTimeTextView = findViewById(R.id.startTimeTextView);
        endTimeTextView = findViewById(R.id.endTimeTextView);
        addressTextView1 = findViewById(R.id.addressTextView1);
        addressTextView2 = findViewById(R.id.addressTextView2);
        additionalRequirementsTextView = findViewById(R.id.additionalRequirementsTextView);
        acceptButton = findViewById(R.id.acceptButton);
        declineButton = findViewById(R.id.declineButton);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptButtonPressed();
            }
        });
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                declineButtonPressed();
            }
        });
        setTextForTextViews();
    }

    private void setTextForTextViews () {
        String full_name = shift.recipient.first_name + " " + shift.recipient.last_name;
        recipientNameTextView.setText(full_name);
        try {
            Date start = serverDateFormat.parse(shift.startShiftTime);
            Date end = serverDateFormat.parse(shift.endShiftTime);
            String startTime = dateFormat.format(start) + " " + timeFormat.format(start);
            String endTime = dateFormat.format(end) + " " + timeFormat.format(end);
            startTimeTextView.setText(startTime);
            endTimeTextView.setText(endTime);

        } catch (ParseException e) {
            Log.d("Time Parse Error:",e.getMessage());
        }
        String[] address_array = shift.address.split("\\,");
        String streetAddress = "";
        String cityAddress = "";
        for (String address : address_array) {
            if (address_array.length > 0 && address.equals(address_array[0])) {
                streetAddress = address;
            }
            else if (address_array.length > 1 && address.equals(address_array[1])) {
                cityAddress = address;
            }
            else if (address_array.length > 0 && !address.equals(address_array[address_array.length - 1])) {
                cityAddress = String.format("%s,%s", cityAddress, address);
            }
        }
        addressTextView1.setText(streetAddress);
        addressTextView2.setText(cityAddress.trim());
        additionalRequirementsTextView.setText(shift.additional_requirements);
    }

    private void hideButtonsAndShowSpinner () {
        acceptButton.setAlpha(0);
        declineButton.setAlpha(0);
        startDialog();
    }

    private void hideSpinnerAndShowButtons () {
        acceptButton.setAlpha(1);
        declineButton.setAlpha(1);
        dismissDialog();
    }


    private void acceptButtonPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Confirm");
        builder.setMessage("By accepting this Shift, you agree to arrive at the stated time and provide services according to recipient's request, and in accordance with all rules and laws rules related to services.");
        builder.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                accept = true;
                sendResponse();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.show();

    }


//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"By accepting this Shift, you agree to arrive at the stated time and provide services according to recipient's request, and in accordance with all rules and laws rules related to services." preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
//    [alert addAction:[UIAlertAction actionWithTitle:@"I Agree" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        NSDate *currentDate = [NSDate date];
//        NSDateFormatter *format = [[NSDateFormatter alloc] init];
//        [format setDateFormat:kDateFormat];
//        NSString *dateStr = [format stringFromDate:currentDate];
//        self.acceptButton.alpha = 0.0;
//        self.declineButton.alpha = self.acceptButton.alpha;
//        [self.activityIndicator startAnimating];
//        [NetworkingHelper postAnRSVPWithPCAID:User.currentUser.pca.idNumber andRequestID:self.request.requestID andETA:dateStr andCancellationReason:@"" andAcceptedBool:YES andDeclinedBool:NO andSuccess:^(id responseObject) {
//            [self showButtonsAndStopActivityIndicator];
//
//            //  NSLog(@"success rsvp %@", responseObject);
//            //       NSMutableArray *mutArr = [NSMutableArray new];
//            //     for (NSDictionary *dict in responseObject) {
//            //       RSVP *rsvp = [[RSVP alloc] initWithDictionary:dict];
//            //        [mutArr addObject:rsvp];
//            //    }
//            //    NSLog(@"mut array %@", mutArr);
//
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been sent to the recipient. You will be notified if they cancel your RSVP." preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self handleSuccessfulAcceptOrDeclineOfRequest];
//            }]];
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//
//
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            [self showButtonsAndStopActivityIndicator];
//            NSLog(@"failure %@", error);
//            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            }]];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                [self dismissViewControllerAnimated:YES completion:NULL];
//            }]];
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//
//        }];
//
//    }]];
//    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//    [self presentViewController:alert animated:YES completion:NULL];

    private void declineButtonPressed() {
        accept = false;
        sendResponse();
    }

    private void sendResponse() {
        boolean acceptance = false;
        boolean declined = true;
        if (accept) {
            acceptance = true;
            declined = false;
        }
        hideButtonsAndShowSpinner();
        ApiService.SendRSVP sendRSVP = apiService.restAdapter.create(ApiService.SendRSVP.class);
        JsonObject rsvp = new JsonObject();
        JsonObject in_rsvp = new JsonObject();
        in_rsvp.addProperty("pca", globals.myPCA.id);
        in_rsvp.addProperty("request", shift.id);
        in_rsvp.addProperty("accepted", acceptance);
        in_rsvp.addProperty("declined", declined);
        rsvp.add("homecare_rsvp", in_rsvp);
        sendRSVP.sendAcceptance(rsvp, sendRSVPResp);
    }

    Callback<ApiService.SimpleResponse> sendRSVPResp = new Callback<ApiService.SimpleResponse>() {
        @Override
        public void success(ApiService.SimpleResponse sendAcceptResp, Response response) {
            if (accept) {
                Toast.makeText(context, "Acceptance Sent", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                builder.setTitle("Success");
                builder.setMessage("Your RSVP has been sent to the recipient. You will be notified if they cancel your RSVP.");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getNext();
                    }
                });
                builder.show();
            }
            else {
                Toast.makeText(context, "Decline Sent", Toast.LENGTH_SHORT).show();
                getNext();
            }
        }

        @Override
        public void failure(RetrofitError code) {
            hideSpinnerAndShowButtons();
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                }
                else {
                    Toast.makeText(context, "Accept Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private void getNext() {
        if (globals.directRequests.size() > 1) {
            hideSpinnerAndShowButtons();
            globals.directRequests.remove(0);
            startActivity(new Intent(context, DirectRequestActivity.class));
            finish();
        }
        else {
            nextActivity();
        }
    }

    public void nextActivity() {
        globals.setLastActive();
        if (globals.hasProfile) {
            switch (globals.whichStart) {
                case 1:
                    if (globals.shiftId != 0) {
                        getShift();
                    } else {
                        dialog.dismiss();
                        startActivity(new Intent(context, FindShiftsActivity.class));
                        finish();
                    }
                    break;
                case 3:
                    dialog.dismiss();
                    startActivity(new Intent(context, FindShiftsActivity.class));
                    break;

                default:
                    dialog.dismiss();
                    startActivity(new Intent(context, SelectActivity.class));
                    finish();
            }

        } else {
            hideSpinnerAndShowButtons();
            startActivity(new Intent(context, ProfileActivity.class));
            finish();
        }
    }

    private void getShift() {
        if (globals.shiftId > 0) {
            ApiService apiService1 = new ApiService(globals.api_key);
            ApiService.GetShift getShift = apiService1.restAdapter.create(ApiService.GetShift.class);
            getShift.getShift(globals.shiftId, setShift);
        }
        else {
            hideSpinnerAndShowButtons();
            startActivity(new Intent(context, SelectActivity.class));
            finish();
        }
    }

    Callback<Shift> setShift = new Callback<Shift>() {
        @Override
        public void success(Shift currentShift, Response response) {
            Geocoder geoCoder = new Geocoder(context);
            boolean isAvailable = true;
            for (RSVP rsvp : currentShift.rsvps) {
                if (isAvailable) {
                    if (rsvp.accepted) {
                        isAvailable = false;
                    }
                }
                if (rsvp.pca.id.equals(globals.myPCA.id)) {
                    currentShift.mine = true;
                }
            }
            if (isAvailable) {
                try {
                    Date start = serverDateFormat.parse(currentShift.startShiftTime);
                    Date end = serverDateFormat.parse(currentShift.endShiftTime);
                    currentShift.timeString = dateFormat.format(start) + " " + timeFormat.format(start) + " - " + timeFormat.format(end);
                    currentShift.distance = "??mi away";
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                if (currentShift.address != null && !currentShift.address.isEmpty()) {
                    try {
                        List<Address> addressList = geoCoder.getFromLocationName(currentShift.address, 1);
                        if (addressList.size() > 0) {
                            currentShift.lat = addressList.get(0).getLatitude();
                            currentShift.lon = addressList.get(0).getLongitude();
                            currentShift.distance = setDistance(currentShift);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            globals.currentShift = currentShift;
            hideSpinnerAndShowButtons();
            startActivity(new Intent(context, ShiftDetailActivity.class));
            finish();
        }
        @Override
        public void failure(RetrofitError code) {
            hideSpinnerAndShowButtons();
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                }
                else {
                    startActivity(new Intent(context, SelectActivity.class));
                    finish();
                }
            }
        }
    };

    public String setDistance(Shift shift) {
        String returnString  = "??mi away";
        if (shift.lat != 0 || shift.lon != 0 && globals.currentLocation != null) {
            Location newSpot = new Location("");
            newSpot.setLatitude(shift.lat);
            newSpot.setLongitude(shift.lon);
            Float distance = globals.currentLocation.distanceTo(newSpot);
            double miles = distance * 0.00062137;
            returnString = (Double.valueOf((double) Math.round(miles * 100d) / 100d)).toString() + "mi away";
        }
        return returnString;
    }

    private void startDialog(){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        //noinspection ConstantConditions
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    private void dismissDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
