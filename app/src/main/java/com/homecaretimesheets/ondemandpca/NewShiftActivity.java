package com.homecaretimesheets.ondemandpca;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.core.content.ContextCompat;
import androidx.percentlayout.widget.PercentRelativeLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class NewShiftActivity extends AppCompatActivity {

    Context context;
    int sdkVersion;
    private ApiService apiService;
    OnDemandGlobal globals;
    OnDemandStorage store;
    Date now;
    Date tooFar;
    Date startTime;
    Date endTime;
    int yearsXp = 0;
    String startString = null;
    String endString = null;
    LinearLayout startSpan;
    LinearLayout endSpan;
    LinearLayout addressSection;
    PercentRelativeLayout genderSection;
    LinearLayout yearsSection;
    TextView qualHead;

    TextView startText;
    TextView endText;
    EditText yearsXpEdit;
    EditText additionalRequirementsEdit;
    String additionalRequirements;
    Spinner genderEdit;
    SpinnerAdapter genAdapter;
    int genderPref = 0;
    String address;
    TextView addressView;
    Button submitBttn;
    ListView qualificationsView;
    ArrayAdapter adapter;
    private List<Qualification> listOpts;
    public List<Qualification> checkedQuals;
    private List<String> checkedList;
    public int qualNum = 0;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 15431;
    boolean setWorker = false;

    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat humanReadableFormat = new SimpleDateFormat("M-d-yy h:mm aa", Locale.US);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = NewShiftActivity.this;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_new_shift);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        apiService = globals.apiService;
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, -1);
        now = c.getTime();
        c.add(Calendar.MONTH, 1);
        tooFar = c.getTime();
        Bundle data = getIntent().getExtras();
        if (data != null) {
            setWorker = data.getBoolean("Selected Worker", false);
        }
        if (setWorker) {
            TextView for_worker = findViewById(R.id.for_worker);
            String headingString = "For Worker: " + globals.myPCA.name;
            for_worker.setText(headingString);
            for_worker.setVisibility(View.VISIBLE);
        }

        Places.initialize(getApplicationContext(), "AIzaSyDUZQzqnze1RNLlhy0MpDzfnCT8IXckkSw");
        PlacesClient placesClient = Places.createClient(this);


        addressView = (TextView) findViewById(R.id.address_view);
        addressSection = (LinearLayout) findViewById(R.id.address_section);
        addressSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressView.setEnabled(false);
                addressBuilder();
            }
        });
        if (store.hasDataString(ODatum.USER_ADDRESS)) {
            address = store.getDataString(ODatum.USER_ADDRESS);
            addressView.setText(address);
        }


        startSpan = (LinearLayout) findViewById(R.id.start_time_holder);
        startSpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                editDateTime(true);
            }
        });


        startText = (TextView) findViewById(R.id.show_start_time);

        endSpan = (LinearLayout) findViewById(R.id.end_time_holder);
        endSpan.setEnabled(false);
        endSpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                editDateTime(false);
            }
        });
        endText = (TextView) findViewById(R.id.show_end_time);
        additionalRequirementsEdit = (EditText) findViewById(R.id.additional_requirement);
        additionalRequirementsEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                additionalRequirements = additionalRequirementsEdit.getText().toString();
            }
        });

        checkedQuals = new ArrayList<>();
        checkedList = new ArrayList<>();
        if (setWorker) {
            genderSection = findViewById(R.id.gender_section);
            genderSection.setVisibility(View.GONE);
            yearsSection = findViewById(R.id.years_xp_holder);
            yearsSection.setVisibility(View.GONE);
            qualHead = findViewById(R.id.qualificationHead);
            qualHead.setVisibility(View.GONE);
        }
        else {
            genderEdit = (Spinner) findViewById(R.id.gender_edit);
            List<OnDemandGlobal.Gender> genders = OnDemandGlobal.Gender.getAllGenders();
            List<String> genStrings = new ArrayList<>();
            for (OnDemandGlobal.Gender gen : genders) {
                genStrings.add(gen.name());
            }
            genAdapter = new ArrayAdapter<>(this, R.layout.small_spinner, genStrings);
            genderEdit.setAdapter(genAdapter);
            genderEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Object selected = parent.getItemAtPosition(position);
                    OnDemandGlobal.Gender gender = OnDemandGlobal.Gender.fromString(selected.toString());
                    if (gender != null) {
                        genderPref = gender.getiValue();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    genderPref = 0;
                }

            });
            yearsXpEdit = (EditText) findViewById(R.id.years_xp_selector);
            yearsXpEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (yearsXpEdit.getText().toString().isEmpty()) {
                        yearsXp = 0;
                    } else {
                        yearsXp = Integer.valueOf(yearsXpEdit.getText().toString());
                    }
                }
            });
            qualificationsView = (ListView) findViewById(R.id.qualification_list);
            qualificationsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            final List<Qualification> listOptions = globals.qualification_list;
            listOpts = listOptions;
            List<String> qualItems = new ArrayList<>();
            if (listOptions != null && !listOptions.isEmpty()) {
                for (Qualification element : listOptions) {
                    qualItems.add(element.qualification.toUpperCase());
                }
            }
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, qualItems);
            qualificationsView.setAdapter(adapter);
            qualificationsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ListView lv = (ListView) parent;
                    Boolean addUp = lv.isItemChecked(position);
                    String qual = lv.getItemAtPosition(position).toString();
                    if (addUp) {
                        addToCheckedList(qual);
                        qualNum++;
                    } else {
                        removeFromCheckedList(qual);
                        qualNum--;
                    }
                    globals.setLastActive();
                }
            });
        }
        submitBttn = (Button) findViewById(R.id.submit_request);
        submitBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkRequest();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recipient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void addressBuilder() {

        List<com.google.android.libraries.places.api.model.Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.ADDRESS);

        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                .build(this);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                address =  place.getAddress().toString();
                addressView.setText(address);
            }
            addressView.setEnabled(true);
        }
    }

    public void editDateTime(final boolean start) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams")
        View dialogLayout = inflation.inflate(R.layout.date_time_dialog, null);
        final TimePicker tp = (TimePicker) dialogLayout.findViewById(R.id.timePicker1);

        final DatePicker dp = (DatePicker) dialogLayout.findViewById(R.id.datePicker1);


        if (!start && !startText.getText().equals("Set Start Time")){
            dp.setMinDate(startTime.getTime());
        }else {
            dp.setMinDate(new Date().getTime() - 1000 );
        }

        Button cancel = (Button) dialogLayout.findViewById(R.id.cancel_button);
        Button doneEdit = (Button) dialogLayout.findViewById(R.id.proceed_button);
        if (sdkVersion < 21) {
            LinearLayout linearLayout = (LinearLayout) dialogLayout.findViewById(R.id.linearLayout1);
            linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.gray4));
        }
        alertDialogBuilder.setView(dialogLayout);
        alertDialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                startSpan.setEnabled(true);
                endSpan.setEnabled(true);
            }
        });
        alertDialogBuilder.setCancelable(false);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startSpan.setEnabled(true);
                endSpan.setEnabled(true);
            }
        });
        doneEdit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                String editedTime;
                int monthInt = dp.getMonth() + 1;
                String monthStr;
                if (monthInt < 10) {
                    monthStr = "0" + monthInt;
                } else {
                    monthStr = Integer.valueOf(monthInt).toString();
                }
                int dayInt = dp.getDayOfMonth();
                String dayStr;
                if (dayInt < 10) {
                    dayStr = "0" + dayInt;
                } else {
                    dayStr = Integer.valueOf(dayInt).toString();
                }
                editedTime = dp.getYear() + "-" + (monthStr) + "-" + dayStr + "T";
                int hourInt;
                int minInt;
                if (sdkVersion > 22) {
                    hourInt = tp.getHour();
                    minInt = tp.getMinute();
                } else {
                    //noinspection deprecation
                    hourInt = tp.getCurrentHour();
                    //noinspection deprecation
                    minInt = tp.getCurrentMinute();
                }
                String hourStr;
                if (hourInt < 10) {
                    hourStr = "0" + hourInt;
                } else {
                    hourStr = "" + hourInt;
                }
                String minStr;
                if (minInt < 10) {
                    minStr = "0" + minInt;
                } else {
                    minStr = "" + minInt;
                }
                TimeZone tz = TimeZone.getDefault();
                int offsetFromUtc = tz.getOffset(now.getTime()) / 3600000;
                String m2tTimeZoneIs = Integer.toString(offsetFromUtc);
                String doubleCheck = editedTime + hourStr + ":" + minStr + ":00-0500";
                editedTime += hourStr + ":" + minStr + ":00" + m2tTimeZoneIs;
                Date editSet = null;
                try {
                    editSet = serverDateFormat.parse(editedTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (editSet == null) {
                    try {
                        editSet = serverDateFormat.parse(doubleCheck);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (editSet == null) {
                    Toast.makeText(context, "Invalid TimeZone", Toast.LENGTH_SHORT).show();
                } else if (checkEdit(editSet, start)) {
                    setTime(editSet,start);
                    alertDialog.dismiss();
                } else {
                    String inStr = humanReadableFormat.format(now.getTime());
                    String outStr = humanReadableFormat.format(tooFar.getTime());
                    Toast.makeText(context, "Time must be after " + inStr + " and before " + outStr, Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.show();
    }

    public boolean checkEdit(Date newTime, boolean start) {
        boolean retBool = false;
        if (now.getTime() < newTime.getTime() && tooFar.getTime() > newTime.getTime()) {
            retBool = true;
            if (!start) {
                if (newTime.getTime() < startTime.getTime()) {
                    retBool = false;
                }
            }
        }
        return retBool;
    }

    public void setTime(Date time, boolean start) {
        if (start) {
            startTime = time;
            startString = serverDateFormat.format(time);
            startSpan.setEnabled(true);
            startText.setTextColor(ContextCompat.getColor(context, R.color.black));
            startText.setText(humanReadableFormat.format(time));
            endSpan.setEnabled(true);
        }
        else {
            endTime = time;
            endString = serverDateFormat.format(time);
            endSpan.setEnabled(true);
            endText.setTextColor(ContextCompat.getColor(context, R.color.black));
            endText.setText(humanReadableFormat.format(time));
        }
    }

    public void checkRequest() {
        boolean canSendRequest = true;
        List<String> issues = new ArrayList<>();
        if (startString == null || endString == null) {
            canSendRequest = false;
            issues.add("* Start Time and End Time must be set");
        }
        if (address == null || address.isEmpty()) {
            canSendRequest = false;
            issues.add("* Set Address");
        }
        TextView requestErrors = (TextView) findViewById(R.id.request_errors);
        if (canSendRequest) {
            sendRequest();
            requestErrors.setVisibility(View.GONE);
        }
        else {
            String displayIssues = TextUtils.join(", ", issues);
            requestErrors.setText(displayIssues);
            requestErrors.setVisibility(View.VISIBLE);
        }
    }

    public void sendRequest() {
        ApiService.SendRequest sendRequest = apiService.restAdapter.create(ApiService.SendRequest.class);
        JsonObject request = new JsonObject();
        store.setDataString(ODatum.USER_ADDRESS, address);
        request.addProperty("recipient", globals.recipient.id);
        request.addProperty("address", address);
        request.addProperty("startShiftTime", startString);
        request.addProperty("endShiftTime", endString);
        request.addProperty("genderPreference", genderPref);
        request.addProperty("yearsOfExperience", yearsXp);
        request.addProperty("additionalRequirements", additionalRequirements);
        if (setWorker) {
            request.addProperty("requested_id", globals.myPCA.id);
        }
        if (qualNum > 0) {
            JsonArray qualIds = getCheckedQualIds();
            request.add("qualifications", qualIds);
        }
        ApiService.ShiftRequestBody requestBody = apiService.new ShiftRequestBody();
        requestBody.homecare_request = request;
        sendRequest.sendRequest(requestBody, sendRequestResp);
    }

    Callback<Void> sendRequestResp = new Callback<Void>() {

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Integer resp_code = 0;
                    if (r != null) {
                        resp_code = r.getStatus();
                    }
                    Toast.makeText(context, "Save Failed: " + resp_code.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void success(Void aVoid, Response response) {
            Toast.makeText(context, "Shift Request Sent", Toast.LENGTH_SHORT).show();
            returnToSelect();
        }
    };

    public void returnToSelect() {
        startActivity(new Intent(context, SelectActivity.class));
        finish();
    }

    public void addToCheckedList(String qual) {
        if (!checkedList.contains(qual)) {
            checkedList.add(qual);
            for (Qualification qualification : listOpts) {
                if (qualification.qualification.toUpperCase().equals(qual)) {
                    if (!checkedQuals.contains(qualification)) {
                        checkedQuals.add(qualification);
                    }
                }
            }
        }
    }

    public void removeFromCheckedList(String qual) {
        if (checkedList.contains(qual)) {
            checkedList.remove(checkedList.indexOf(qual));
            for (Qualification qualification : listOpts) {
                if (qualification.qualification.toUpperCase().equals(qual)) {
                    if (checkedQuals.contains(qualification)) {
                        checkedQuals.remove(checkedQuals.indexOf(qualification));
                    }
                }
            }
        }
    }

    public JsonArray getCheckedQualIds() {
        JsonArray idList = new JsonArray();
        for (Qualification item : listOpts) {
            if (checkedQuals.contains(item)) {
                idList.add(item.id);
            }
        }
        return idList;
    }

    @Override
    public void onBackPressed() {
        returnToSelect();
    }
}
