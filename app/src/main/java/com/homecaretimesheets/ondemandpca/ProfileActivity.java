package com.homecaretimesheets.ondemandpca;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
//import android.support.annotation.NonNull;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


public class ProfileActivity extends AppCompatActivity {
    Context context;
    private int sdkVersion;
    private OnDemandGlobal globals;
    private NonScrollListView qualificationsView;
    private List<Qualification> listOpts;
    private onDemandPCA pca;
    private List<String> checkedList;
    public ArrayAdapter<String> adapter;
    public ArrayAdapter<String> genAdapter;
    public EditText firstNameEdit;
    public EditText lastNameEdit;
    public EditText yearsExpEdit;
    public EditText aboutMeEdit;
    public String aboutMe;
    Spinner genderEdit;
    public String firstname = "";
    public String lastname = "";
    public String yearsExp = "";
    public String genderName = "";
    public ImageView imgView;
    private MenuItem menuNext = null;
    public List<Qualification> checkedQuals;
    private List<County> countOpts;
    public List<County> checkedCounties;
    private NonScrollListView countiesView;
    private List<String> checkedCList;
    public ArrayAdapter<String> county_adapter;
    public int countyNum = 0;
    public int picCode = 59292;
    public ApiService apiService;
    public File profileImage;

    public int heightOfImage = 0;
    public int widthOfImage = 0;

    public Dialog dialog;
    public int qualNum = 0;
    public Integer pcaId = 0;
    private static final int REQUEST_CAMERA_PERMISSION = 1;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = ProfileActivity.this;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_profile);
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }
        globals = (OnDemandGlobal) getApplicationContext();
        pca = globals.myPCA;
        pcaId = globals.user.id;
        apiService = globals.apiService;
        qualificationsView = (NonScrollListView) findViewById(R.id.qualifications_list);
        qualificationsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        final List<Qualification> listOptions = globals.qualification_list;
        listOpts = listOptions;
        List<String> qualItems = new ArrayList<>();
        for (Qualification element : listOptions) {
            qualItems.add(element.qualification.toUpperCase());
        }
        checkedQuals = new ArrayList<>();
        checkedList = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, qualItems);
        qualificationsView.setAdapter(adapter);
        repopLists();
        qualificationsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView lv = (ListView) parent;
                Boolean addUp = lv.isItemChecked(position);
                String qual = lv.getItemAtPosition(position).toString();
                if (addUp) {
                    addToCheckedList(qual);
                    qualNum++;
                }
                else {
                    removeFromCheckedList(qual);
                    qualNum--;
                }
                globals.setLastActive();
                checkSubmit();
            }
        });
        countiesView = (NonScrollListView) findViewById(R.id.counties_list);
        countiesView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        final List<County> countyOptions = globals.counties;
        countOpts = countyOptions;
        List<String> countItems = new ArrayList<>();
        for (County element : countyOptions) {
            countItems.add(element.name.toUpperCase());
        }
        checkedCounties = new ArrayList<>();
        checkedCList = new ArrayList<>();
        county_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, countItems);
        countiesView.setAdapter(county_adapter);

        repopCountyLists();
        countiesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView lv = (ListView) parent;
                Boolean addUp = lv.isItemChecked(position);
                String county = lv.getItemAtPosition(position).toString();
                if (addUp) {
                    addToCountyList(county);
                    countyNum++;
                }
                else {
                    removeFromCountyList(county);
                    countyNum--;
                }
                globals.setLastActive();
                checkSubmit();
            }
        });
        genderEdit = (Spinner) findViewById(R.id.gender_edit);
        List<OnDemandGlobal.Gender> genders = OnDemandGlobal.Gender.getGenders();
        List<String> genStrings = new ArrayList<>();
        for (OnDemandGlobal.Gender gen : genders) {
            genStrings.add(gen.name());
        }
        genAdapter = new ArrayAdapter<>(this, R.layout.small_spinner, genStrings);
        genderEdit.setAdapter(genAdapter);
        if (pca != null && pca.gender != null && pca.gender != 0) {
            genderName = OnDemandGlobal.Gender.fromInt(pca.gender).name();
            int spinnerPosition = genAdapter.getPosition(genderName);
            genderEdit.setSelection(spinnerPosition);
        }
        genderEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object selected = parent.getItemAtPosition(position);
                OnDemandGlobal.Gender gender = OnDemandGlobal.Gender.fromString(selected.toString());
                if (gender != null) {
                    pca.gender = gender.getiValue();
                    genderName = selected.toString();
                    checkSubmit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                pca.gender = 0;
            }

        });
        firstNameEdit = (EditText) findViewById(R.id.first_name_edit);
        if (pca.first_name != null && !"".equals(pca.first_name)) {
            firstname = globals.upCaseWords(pca.first_name);
            firstNameEdit.setText(firstname);
        }
        firstNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                globals.setLastActive();
                firstname = firstNameEdit.getText().toString();
                checkSubmit();
            }
        });

        lastNameEdit = (EditText) findViewById(R.id.last_name_edit);
        if (pca.last_name != null && !"".equals(pca.last_name)) {
            lastname = globals.upCaseWords(pca.last_name);
            lastNameEdit.setText(lastname);
        }
        lastNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                globals.setLastActive();
                lastname = lastNameEdit.getText().toString();
                checkSubmit();
            }
        });
        aboutMeEdit = (EditText) findViewById(R.id.about_body);
        if (pca.about_me != null && !"".equals(pca.about_me)) {
            aboutMe = pca.about_me;
            aboutMeEdit.setText(aboutMe);
        }
        aboutMeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                globals.setLastActive();
                aboutMe = aboutMeEdit.getText().toString();
                checkSubmit();
            }
        });

        yearsExpEdit = (EditText) findViewById(R.id.year_edit);
        if (pca.years_of_experience != null) {
            yearsExp = pca.years_of_experience.toString();
            yearsExpEdit.setText(yearsExp);
        }
        else {
            pca.years_of_experience = 1;
            yearsExp = pca.years_of_experience.toString();
        }
        yearsExpEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                globals.setLastActive();
                yearsExp = yearsExpEdit.getText().toString();
                checkSubmit();
            }
        });

        imgView = (ImageView) findViewById(R.id.profileImage);

        LinearLayout imgHolder = (LinearLayout) findViewById(R.id.edit_profile_image);
        if (globals.hasProfileImg) {
            imgView.setImageURI(Uri.fromFile(globals.profileImg));
        }
        imgHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCamera();
            }
        });

    }


    public void getCamera(){
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            getImage();
        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    public void getImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, picCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
                    throw new RuntimeException("Error on requesting camera permission.");
                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setCancelable(false);
                    builder.setTitle("Camera Permission Failed");
                    builder.setMessage("Camera permissions are required for the functioning of this app. Please go to settings and allow camera permissions.");
                    builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
                else {
                    getImage();
                }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == picCode && resultCode == Activity.RESULT_OK) {
            setLoadingDialog();
            String fileName = globals.profileFileName;
            OutputStream os = null;
            try {
                Bitmap finalBM = (Bitmap) data.getExtras().get("data");

                Uri uri = getImageUri(this, finalBM);

                os = openFileOutput(fileName, Context.MODE_PRIVATE);
                if (finalBM != null) {

                    finalBM = handleSamplingAndRotationBitmap(this, uri);
                    finalBM.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    if (globals.fileExistance(fileName)) {
                        globals.profileImg = new File(getFilesDir(),fileName);
                        profileImage = globals.profileImg;
                        globals.hasProfileImg = true;
                    }
                    imgView.setImageBitmap(finalBM);

                }
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException e) {
                    // Ignore
                    }
                }
            }
            sendProfileImage();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        if (getHeightOfImage(options) < gerWidthOfImage(options)){
            img = rotateImageIfRequired(context, img, selectedImage);
        }

        return img;
    }

    public static int getHeightOfImage(BitmapFactory.Options options) {
        final int height = options.outHeight;
        return height;
    }


    public static int gerWidthOfImage(BitmapFactory.Options options) {
        final int width = options.outWidth;
        return width;
    }

        public static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;



        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateThisImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateThisImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateThisImage(img, 270);
            default:
                return rotateThisImage(img, 90);
        }
    }

    private static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            cursor.close();
            return -1;
        }

        cursor.moveToFirst();
        int orientation = cursor.getInt(0);
        cursor.close();
        cursor = null;
        return orientation;
    }

    public static Bitmap rotateBitmap(Context context, Uri photoUri, Bitmap bitmap) {
        int orientation = getOrientation(context, photoUri);
        if (orientation <= 0) {
            return bitmap;
        }
        Log.e("orientation is", "value"+orientation);
        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        return bitmap;
    }

    private static Bitmap rotateThisImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public static Bitmap rotateTheImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void setLoadingDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        //noinspection ConstantConditions
        dialog.getWindow().setBackgroundDrawableResource(R.color.clear);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    protected void sendProfileImage() {
        ApiService apiService = new ApiService(globals.api_key);
        ApiService.SendProfileImage sendupImg = apiService.restAdapter.create(ApiService.SendProfileImage.class);
        TypedFile proFile = new TypedFile("multipart/form-data", globals.profileImg);
        sendupImg.setImage(globals.myPCA.id, proFile, sendImageResponse);
    }

    Callback<ApiService.SimpleResponse> sendImageResponse = new Callback<ApiService.SimpleResponse>() {

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.fromRefresh = false;
                }
            }
            dialog.dismiss();
        }

        @Override
        public void success(ApiService.SimpleResponse inFileResp, Response response) {
            globals.setProfileImage();
            dialog.dismiss();
        }
    };


    public static Bitmap rotateImage(Bitmap source) {
        int width  = source.getWidth();
        int height = source.getHeight();
        int newWidth = (height > width) ? width : height;
        int newHeight = (height > width)? height - ( height - width) : height;
        int cropW = (width - height) / 2;
        cropW = (cropW < 0)? 0: cropW;
        int cropH = (height - width) / 2;
        cropH = (cropH < 0)? 0: cropH;
        Matrix matrix = new Matrix();
//        matrix.postRotate(90);
        Bitmap almostThere = Bitmap.createBitmap(source, cropW, cropH, newWidth, newHeight, matrix, true);
        return Bitmap.createScaledBitmap(almostThere, 500, 500, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit, menu);
        menuNext = menu.findItem(R.id.next_bar);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.next_bar) {
            storeProfile();
            return true;
        }
        if (id == R.id.cancel_bar) {
            onBackPressed();
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void repopLists() {
        List<Qualification> selectedQuals = pca.qualifications == null ? new ArrayList<Qualification>() : pca.qualifications;
        if (selectedQuals.size() > 0) {
            for(int i=0; i < selectedQuals.size(); i++) {
                Qualification thisQual = selectedQuals.get(i);
                qualificationsView.setItemChecked(adapter.getPosition(thisQual.qualification.toUpperCase()), true);
                qualNum++;
                addToCheckedList(thisQual.qualification.toUpperCase());
            }
        }
    }

    public void addToCheckedList(String qual) {
        if (!checkedList.contains(qual)) {
            checkedList.add(qual);
            for (Qualification qualification : listOpts) {
                if (qualification.qualification.toUpperCase().equals(qual)) {
                    if (!checkedQuals.contains(qualification)) {
                        checkedQuals.add(qualification);
                    }
                }
            }
        }
    }

    public void removeFromCheckedList(String qual) {
        if (checkedList.contains(qual)) {
            checkedList.remove(checkedList.indexOf(qual));
            for (Qualification qualification : listOpts) {
                if (qualification.qualification.toUpperCase().equals(qual)) {
                    if (checkedQuals.contains(qualification)) {
                        checkedQuals.remove(checkedQuals.indexOf(qualification));
                    }
                }
            }
        }
    }

    public JsonArray getCheckedQualIds() {
        JsonArray idList = new JsonArray();
        for (Qualification item : listOpts) {
            if (checkedQuals.contains(item)) {
                idList.add(Integer.valueOf(item.id).toString());
            }
        }
        return idList;
    }

    public void repopCountyLists() {
        List<County> selectedCounties = globals.counties == null ? new ArrayList<County>() : pca.counties;
        if (selectedCounties.size() > 0) {
            for(int i=0; i < selectedCounties.size(); i++) {
                County thisCounty = selectedCounties.get(i);
                String thisName = thisCounty.name.toUpperCase();
                countiesView.setItemChecked(county_adapter.getPosition(thisName), true);
                countyNum++;
                addToCountyList(thisName);
            }
        }
    }

    public void addToCountyList(String count) {
        if (!checkedCList.contains(count)) {
            checkedCList.add(count);
            for (County county : countOpts) {
                if (county.name.toUpperCase().equals(count)) {
                    if (!checkedCounties.contains(county)) {
                        checkedCounties.add(county);
                    }
                }
            }
        }
    }

    public void removeFromCountyList(String count) {
        if (checkedCList.contains(count)) {
            checkedCList.remove(checkedCList.indexOf(count));
            for (County county : countOpts) {
                if (county.name.toUpperCase().equals(count)) {
                    if (checkedCounties.contains(county)) {
                        checkedCounties.remove(checkedCounties.indexOf(county));
                    }
                }
            }
        }
    }

    public JsonArray getCheckedCountyIds() {
        JsonArray idList = new JsonArray();
        for (County item : countOpts) {
            if (checkedCounties.contains(item)) {
                idList.add(Integer.valueOf(item.id).toString());
            }
        }
        return idList;
    }

    protected void storeProfile() {
        if (!globals.hasProfileImg) {
            Toast.makeText(context, "Profile Image Required", Toast.LENGTH_LONG).show();
        }
        else {
            ApiService.SetPCAProfile setPCAProfile = apiService.restAdapter.create(ApiService.SetPCAProfile.class);
            JsonObject profile = new JsonObject();
            JsonArray qualIds = getCheckedQualIds();
            profile.add("qualifications", qualIds);
            profile.addProperty("firstName", globals.upCaseWords(firstname));
            profile.addProperty("lastName", globals.upCaseWords(lastname));
            profile.addProperty("aboutMe", aboutMe);
            JsonArray countyIds = getCheckedCountyIds();
            profile.add("counties", countyIds);
            profile.addProperty("gender", pca.gender);
            int num = 0;
            try {
                num = Integer.parseInt(yearsExp);
            }catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (num > 0) {
                yearsExp = Integer.valueOf(num).toString();
            }
            else {
                yearsExp = "1";
            }
            profile.addProperty("yearsOfExperience", yearsExp);
            ApiService.SetPCAProfileBody setPCAProfileBody = apiService.new SetPCAProfileBody();
            setPCAProfileBody.homecare_profile = profile;
            setPCAProfile.setProfile(pca.id, setPCAProfileBody, getPCAProfileResp);
        }
    }

    Callback<ApiService.SimpleResponse> getPCAProfileResp = new Callback<ApiService.SimpleResponse>() {
        @Override
        public void success(ApiService.SimpleResponse setPCAProfileResp, Response response) {
            globals.hasProfile = true;
            globals.getPCAData(getPCAResp);
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
                finish();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    protected Callback<onDemandPCA> getPCAResp = new Callback<onDemandPCA>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
                globals.resetLogin(context);
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "User Data Failed", sdkVersion).show();
                }
            }
        }
        @Override
        public void success(onDemandPCA pca, Response response) {
            globals.setPca(pca);
            returnToShifts();
        }
    };

    public void returnToShifts() {
        Intent intent;
        if (globals.user.isPca) {
            intent = new Intent(context, FindShiftsActivity.class);
        }
        else {
            intent = new Intent(context, MyShiftsActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void checkSubmit() {
        Boolean doSubmit = true;
        if ("".equals(firstname)) {
            doSubmit = false;
        }
        if ("".equals(lastname)) {
            doSubmit = false;
        }
        if ("".equals(genderName)) {
            doSubmit = false;
        }
        if ("".equals(yearsExp)) {
            doSubmit = false;
        }
        menuNext.setEnabled(doSubmit);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, SelectActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
