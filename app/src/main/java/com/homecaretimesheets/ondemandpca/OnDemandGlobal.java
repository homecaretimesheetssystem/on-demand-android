package com.homecaretimesheets.ondemandpca;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.StrictMode;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit.Callback;

public class OnDemandGlobal extends Application {
    public User user;
    public static int CONNECT_REQUEST = 10001;
    public static int UPDATE_REQUEST = 10011;
    public static int NETWORK_REQUEST = 10014;
    public static int INTERNET_REQUEST = 10015;
    public boolean netPerm;
    public boolean inetPerm;
    public boolean locPerm;
    public boolean storPerm;
    public List<Qualification> qualification_list;
    public Date lastActive;
    public String api_key;
    public Agency agency;
    public String amazonUrl;
    public boolean needsProfileImg = false;
    public boolean hasProfileImg = false;
    public String profileBaseName = "profile";
    public String profileFileName = "";
    public File profileImg;
    public boolean hasProfile = false;
    public boolean fromRefresh = false;
    public onDemandPCA myPCA;
    public ApiService apiService;
    public List<Shift> shifts;
    public Shift currentShift;
    public File agencyImage;
    public boolean hasAgencyImg = false;
    String agencyImageUrl;
    public Location currentLocation = null;
    public List<County> counties;
    public Recipient recipient;
    public onDemandPCA current_pca;
    public boolean back_to_find = true;
    public String deviceToken = null;
    public int whichStart = 0;
    public int shiftId = 0;
    public PCA pca;
    public Integer currentActivity = 0;
    RefreshVariable freshVariable;
    public List<onDemandPCA> workers;
    public boolean back_to_shift = false;
    public List<Shift> directRequests = new ArrayList<>();

    public boolean testing = false;

    enum Gender {
        NoPreference(0),
        Female(1),
        Male(2);
        private int _iValue = 0;
        private Integer[] genders = {1, 2};

        Gender(int value) {
            this._iValue = value;
        }

        int getiValue() {
            return this._iValue;
        }

        public static List<Gender> getGenders() {
            List<Gender> genderList = new ArrayList<>();
            for (Gender b : Gender.values()) {
                if (b.isGender()) {
                    genderList.add(b);
                }
            }
            return genderList;
        }

        public static List<Gender> getAllGenders() {
            List<Gender> genderList = new ArrayList<>();
            Collections.addAll(genderList, Gender.values());
            return genderList;
        }

        public static Gender fromString(String value) {
            for (Gender b : Gender.values()) {
                if (b.name().equals(value)) {
                    return b;
                }
            }
            return Gender.fromInt(0);
        }

        public static Gender fromInt(int value) {
            for (Gender b : Gender.values()) {
                if (b.getiValue() == value) {
                    return b;
                }
            }
            return Gender.fromInt(0);
        }

        boolean isGender() {
            return Arrays.asList(genders).contains(this._iValue);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        user = new User();
        netPerm = false;
        inetPerm = false;
        locPerm = false;
        storPerm = false;
        lastActive = new Date();
        freshVariable = new RefreshVariable();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public void setAgencyImage (File image) {
        agencyImage = image;
        hasAgencyImg = true;
    }

    public void setDirectRequests(List<Shift> requests) {
        directRequests = requests;
    }

    public void setLastActive() {
        lastActive = new Date();
    }

    public boolean checkLastActive() {
        Date longTime = new Date(System.currentTimeMillis() - 120000);
        return longTime.after(lastActive);
    }

    public void setApiToken(String apiToken) {
        api_key = apiToken;
    }

    public void setPca(onDemandPCA pca) {
        myPCA = pca;
        boolean hasFullProfile = false;
        boolean isFullProfile = true;
        if (myPCA.first_name == null || myPCA.first_name.isEmpty()) {
            isFullProfile = false;
        }
        else {
            myPCA.first_name = upCaseWords(pca.first_name);
        }
        if (myPCA.last_name == null || myPCA.last_name.isEmpty()) {
            isFullProfile = false;
        }
        else {
            myPCA.last_name = upCaseWords(pca.last_name);
        }
        if (myPCA.gender == null || !Gender.fromInt(myPCA.gender).isGender()) {
            isFullProfile = false;
        }
        if (myPCA.years_of_experience == null || myPCA.years_of_experience < 0) {
            isFullProfile = false;
        }
        if (myPCA.profilePhoto != null && !myPCA.profilePhoto.isEmpty()) {
            needsProfileImg = true;
        }
        else {
            isFullProfile = false;
        }
        if (isFullProfile) {
            hasFullProfile = true;
        }
        profileFileName = profileBaseName + myPCA.id + ".jpg";
        hasProfile = hasFullProfile;
    }

    public boolean setRecipient(Recipient recip) {
        recipient = recip;
        return true;
    }

    public boolean setUser(ApiService.DataResponse data) {
        user.setUser(data.user);
        if (!user.isPca) {
            setRecipient(data.recipient);
        }
        else {
            pca = data.pca;
            directRequests = data.pca.direct_requests;
        }
        user.setAgency(data.agency);
        agency = data.agency;
        amazonUrl = data.amazonUrl;
        String img_url = data.amazonUrl + agency.image_name;
        img_url = img_url.replace("\"", "");
        agencyImageUrl = img_url;
        return true;
    }

    public boolean fileExistance(String fname){
        File file = new File(getFilesDir(), fname);
        return file.exists();
    }

    public Date getStartOfDay() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 0, 0, 0);
        return calendar.getTime();
    }

    public boolean setProfileImage() {
        boolean hasFile = fileExistance(profileFileName);
        if (hasFile) {
            profileImg = new File(getFilesDir(),profileFileName);
        }
        hasProfileImg = hasFile;
        return hasFile;
    }

    public String upCaseWords(String str) {
        Log.d("Upcase",str);
        if (str.length() > 0) {
            while(str.contains("  ")) {
                str = str.replace("  ", " ");
            }
            String[] strArray = str.split(" ");
            StringBuilder builder = new StringBuilder();
            for (String s : strArray) {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                builder.append(cap).append(" ");
            }
            return builder.toString();
        }
        else {
            return str;
        }
    }

    public void getData(Callback<ApiService.DataResponse> callback) {
        user = new User();
        apiService = new ApiService(api_key);
        ApiService.DataService datafetch = apiService.restAdapter.create(ApiService.DataService.class);
        datafetch.getData(callback);
    }

    public void getPCAData(Callback<onDemandPCA> callback) {
        ApiService.GetPCA  getPCA = apiService.restAdapter.create(ApiService.GetPCA.class);
        getPCA.getData(pca.id, callback);
    }

    protected void getQualificationList(Callback<List<Qualification>> callback) {
        ApiService.QualificationsList qualificationGet = apiService.restAdapter.create(ApiService.QualificationsList.class);
        qualificationGet.getQualifications(callback);
    }

    public void setQualificationList(List<Qualification> quals) {
        qualification_list = quals;
    }

    protected void getCounties(Callback<List<County>> callBack) {
        ApiService.GetCounties getCountyList = apiService.restAdapter.create(ApiService.GetCounties.class);
        getCountyList.getCounties(callBack);
    }

    public void setCounties(List<County> countys) { counties = countys;}

    public void resetLogin(Context context) {
        fromRefresh = false;
        OnDemandStorage store = new OnDemandStorage(context);
        store.clearDatum(ODatum.REFRESH_KEY);
        store.clearDatum(ODatum.USER_ADDRESS);
        Intent intent = new Intent(context, Splash.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public onDemandPCA getWorker(Integer id) {
        onDemandPCA worker = null;
        if (workers != null) {
            if (workers.size() > 0) {
                worker = workers.get(id);
            }
        }
        return worker;
    }

    public void getStart(Context context) {

    }
}

