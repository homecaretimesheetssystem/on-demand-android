package com.homecaretimesheets.ondemandpca;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;


public class SelectActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{
    Context context;
    private int sdkVersion;
    private OnDemandGlobal globals;
    OnDemandStorage store;
    User user;
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;
    int connectRequest = OnDemandGlobal.CONNECT_REQUEST;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = SelectActivity.this;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_select);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        user = globals.user;
        Button selectShifts = (Button) findViewById(R.id.find_shifts_select);
        Button myShifts = (Button) findViewById(R.id.my_shifts_select);
        myShifts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MyShiftsActivity.class));
                finish();
            }
        });
        Button browseSelect = (Button) findViewById(R.id.browse_workers);
        browseSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, BrowseWorkersActivity.class));
                finish();
            }
        });
        if (user.isPca) {
            browseSelect.setVisibility(View.GONE);
            checkProfile();
            myShifts.setText(R.string.my_shifts);
            selectShifts.setText(R.string.connect_to_shifts);
            selectShifts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, FindShiftsActivity.class));
                    finish();
                }
            });
            checkLocationPermission();
            buildGoogleApiClient();
            mGoogleApiClient.connect();
            createLocationRequest();
        }
        else {
            myShifts.setText(R.string.see_requests);
            selectShifts.setText(R.string.request_pca);
            selectShifts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, NewShiftActivity.class));
                    finish();
                }
            });
        }
        if (globals.hasAgencyImg && globals.agencyImage != null) {
            ImageView agencyIcon = (ImageView) findViewById(R.id.agency_icon);
            agencyIcon.setImageURI(Uri.fromFile(globals.agencyImage));
        }
        else {
            new SetImageTask(globals).execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (globals.user.isPca) {
            getMenuInflater().inflate(R.menu.pca, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.recipient, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.prof_bar) {
            goToProfile();
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkProfile() {
        if (!globals.hasProfile) {
            Toast.makeText(context, "Complete your Profile before using OnDemand Services", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, ProfileActivity.class));
            finish();
        }
    }

    private void goToProfile() {
        startActivity(new Intent(context,ProfileActivity.class));
        finish();
    }

    private class SetImageTask extends AsyncTask<Void,Void,File> {
        OnDemandGlobal globals;

        private SetImageTask(OnDemandGlobal global) {
            this.globals = global;
        }
        @Override
        protected File doInBackground(Void... params) {
            File file;
            do {
                file = globals.agencyImage;
            }
            while (!globals.hasAgencyImg);
            return file;
        }

        protected void onPostExecute(File image) {
            ImageView agencyIcon = (ImageView) findViewById(R.id.agency_icon);
            if (globals.agencyImage != null) {
                agencyIcon.setImageURI(Uri.fromFile(globals.agencyImage));
            }
        }
    }

    protected void onStart() {
        super.onStart();
        globals.currentActivity = 3;
    }

    public void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    connectRequest);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == connectRequest) {
            if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
                createLocationRequest();
            }
            else {
                globals.resetLogin(context);
                finish();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnectionSuspended(int arg0) {}

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@Nullable ConnectionResult arg0) {
        Toast.makeText(context, "Location Service Failed", sdkVersion).show();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        int UPDATE_INTERVAL = 10000; // 10 sec
        int FATEST_INTERVAL = 5000; // 5 sec
        int DISPLACEMENT = 100; // 100 meters
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, connectRequest);
            return;
        }
        LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
        globals.currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onRestart() {

        if (user.isPca) {
            buildGoogleApiClient();
            createLocationRequest();
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        }
        super.onRestart();
    }

    @Override
    public void onLocationChanged(Location location) {
        globals.currentLocation = location;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	connectRequest);
            return;
        }
        createLocationRequest();
        startLocationUpdates();
    }
}
