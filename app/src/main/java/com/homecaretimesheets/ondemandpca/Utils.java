package com.homecaretimesheets.ondemandpca;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;

public class Utils {
    @SuppressLint("NewApi")
    static void verifyNetworkConnection(final Context context, int version) {
        if (!isNetworkConnected(context)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Network Disconnected")
                    .setMessage("This app needs a Wi-Fi or mobile data connection." +
                            "Would you like to check your settings?")
                    .setNeutralButton("4G", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intentThreeG = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                            context.startActivity(intentThreeG);
                            dialog.cancel();
                        }
                    })
                    .setPositiveButton("Wi-Fi", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intentWiFi = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            context.startActivity(intentWiFi);
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();

            if (((Activity) context).isFinishing()) {
                return;
            }

            if ((version > android.os.Build.VERSION_CODES.JELLY_BEAN) &&
                    ((Activity) context).isDestroyed()) {
                return;
            }

            alert.show();
        }
    }

    @SuppressWarnings("deprecation")
    static Boolean isNetworkConnected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean wifiCon = false;
        boolean mobileCon = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            NetworkInfo netInfo = connManager.getActiveNetworkInfo();
            if (netInfo != null) {
                int netInfoType = netInfo.getType();
                wifiCon = netInfoType == ConnectivityManager.TYPE_WIFI;
                mobileCon = netInfoType == ConnectivityManager.TYPE_MOBILE;
            }
        }
        else {
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mMobile = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            wifiCon = mWifi.isConnected();
            mobileCon = mMobile.isConnected();
        }
        return wifiCon || mobileCon;
    }

    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            return packageManager.getApplicationInfo(packagename, 0).enabled;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
