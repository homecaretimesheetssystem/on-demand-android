package com.homecaretimesheets.ondemandpca;

import android.content.Context;
import android.content.SharedPreferences;


public class OnDemandStorage {
    public static final int INVALID_INT_VALUE = -1;
    public static final String INVALID_STRING_VALUE = "ERROR";

    private final SharedPreferences mPrefs;
    private static final String TIME_SHEET_PREFERENCES =
            "OnDemandPreferences";

    public OnDemandStorage(Context context) {
        mPrefs = context.getSharedPreferences(
                TIME_SHEET_PREFERENCES,
                Context.MODE_PRIVATE);
    }


    public void setDataInt(ODatum datum, int data) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(getFieldKey(datum), data);
        editor.commit();
    }

    public int getDataInt(ODatum datum) {
        return mPrefs.getInt(getFieldKey(datum),
                INVALID_INT_VALUE);
    }

    public Boolean hasDataInt(ODatum datum) {
        return (mPrefs.getInt(getFieldKey(datum),
                INVALID_INT_VALUE) != INVALID_INT_VALUE);
    }

    public void setDataBoolean(ODatum datum, Boolean data) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(getFieldKey(datum), data);
        editor.commit();
    }

    public Boolean getDataBoolean(ODatum datum) {
        return mPrefs.getBoolean(getFieldKey(datum), false);
    }

    public void setDataString(ODatum datum, String value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(getFieldKey(datum), value);
        editor.commit();
    }

    public String getDataString(ODatum datum) {
        return mPrefs.getString(getFieldKey(datum),
                INVALID_STRING_VALUE);
    }

    public Boolean hasDataString(ODatum datum) {
        return (!mPrefs.getString(getFieldKey(datum),
                INVALID_STRING_VALUE).equals(INVALID_STRING_VALUE));
    }

    public void clearData() {
        SharedPreferences.Editor editor = mPrefs.edit();
        for (ODatum datum : ODatum.values()) {
            editor.remove(getFieldKey(datum));
        }
        editor.commit();
    }

    public void clearDatum(ODatum datum) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(getFieldKey(datum));
        editor.commit();
    }

    private String getFieldKey(ODatum datum) {
        return "KEY_" + Integer.toString(datum.ordinal());
    }

}
