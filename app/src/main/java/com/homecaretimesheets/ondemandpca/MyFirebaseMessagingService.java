package com.homecaretimesheets.ondemandpca;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        final OnDemandGlobal globals = (OnDemandGlobal) getApplicationContext();
        String message = null;
        if (remoteMessage.getNotification() != null) {
             message = remoteMessage.getNotification().getBody();
        }
        if (remoteMessage.getData().containsKey("request_id")) {
            globals.shiftId = Integer.valueOf(remoteMessage.getData().get("request_id"));
        }
        if (remoteMessage.getData().containsKey("push_type")) {
            globals.whichStart = Integer.valueOf(remoteMessage.getData().get("push_type"));
        }
        final Context context = getApplicationContext();
        if (globals.whichStart == 1) {
            if (message == null) {
                message = "New Pca Request Available";
            }
            if (globals.currentActivity == 4) {
               Handler h = new Handler(Looper.getMainLooper());
               h.post(new Runnable() {
                   public void run() {
                       globals.freshVariable.setRefresh(true);
                   }
               });
           }
        }
        else if (globals.whichStart == 2) {
            if (message == null) {
              message = "A Pca Accepted Your Request";
            }
            if  (globals.currentActivity == 6) {
                if (globals.currentShift.id == globals.shiftId) {
                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        public void run() {
                            globals.freshVariable.setRefresh(true);
                        }
                    });
                }
            }
            else if (globals.currentActivity == 7) {
                Handler h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        globals.freshVariable.setRefresh(true);
                    }
                });
            }
        }
        else if (globals.whichStart == 3) {
            if (message == null) {
                message = "Recipient Cancelled Shift";
            }
            if (globals.currentActivity == 5 || globals.currentActivity == 7 || globals.currentActivity == 4) {
                Handler h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        globals.freshVariable.setRefresh(true);
                    }
                });
            }
        }
        else if (globals.whichStart == 4) {
            if (message == null) {
                message = "Pca Cancelled Shift";
            }
            if (globals.currentActivity == 6 || globals.currentActivity == 7) {
                Handler h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        globals.freshVariable.setRefresh(true);
                    }
                });
            }
        }
        if (message != null) {
            final String newMessage = message;
            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    Toast.makeText(context, newMessage, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}