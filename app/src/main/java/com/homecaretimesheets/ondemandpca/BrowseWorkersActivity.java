package com.homecaretimesheets.ondemandpca;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BrowseWorkersActivity extends AppCompatActivity {

  Context context;
  private int sdkVersion;
  private OnDemandGlobal globals;
  OnDemandStorage store;
  User user;
  ApiService apiService;
  Dialog dialog;
  GridView workerGrid;
  EditText searchWorkers;
  TextView no_workers;


    protected void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        context = BrowseWorkersActivity.this;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_browse_workers);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        apiService = globals.apiService;
        user = globals.user;
        workerGrid = findViewById(R.id.grid_worker);
        no_workers = findViewById(R.id.no_workers);
        globals.back_to_shift = false;
        setLoadingDialog();
        getWorkers();
        searchWorkers = (EditText) findViewById(R.id.search_workers);
        searchWorkers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterWorkers(searchWorkers.getText().toString());
            }
        });
    }

    public void filterWorkers(String string) {
        if (globals.workers.size() < 1) {
            setNoWorkers();
        }
        else {
            List<onDemandPCA> pca_list = new ArrayList<>();
            for(onDemandPCA worker : globals.workers) {
                boolean add = false;
                String worker_name = (worker.first_name + " " + worker.last_name).toLowerCase();
                if (worker_name.contains(string.toLowerCase())) {
                    add = true;
                }
                if (add) {
                    pca_list.add(worker);
                }
            }
            if (pca_list.size() > 0) {
                setWorkers(pca_list);
            }
            else {
                setNoWorkers();
            }
         }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recipient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void setWorkers(final List<onDemandPCA> pcas) {
        no_workers.setVisibility(View.GONE);
        workerGrid.setVisibility(View.VISIBLE);
        WorkerGridAdapter gridAdapter = new WorkerGridAdapter(context, pcas);
        workerGrid.setAdapter(gridAdapter);
        workerGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToProfile(pcas.get(position));
            }
        });
    }

    void goToProfile(onDemandPCA pca) {
        globals.myPCA = pca;
        startActivity(new Intent(context, ViewProfileActivity.class));
        finish();
    }

    void setNoWorkers() {
        no_workers.setVisibility(View.VISIBLE);
        workerGrid.setVisibility(View.GONE);
    }

    void getWorkers() {
        ApiService.GetPcaContacts get_workers = apiService.restAdapter.create(ApiService.GetPcaContacts.class);
        get_workers.getContacts(showContacts);
    }

    protected Callback<ApiService.ContactResponse> showContacts = new Callback<ApiService.ContactResponse>() {
        @Override
        public void success(ApiService.ContactResponse contactResponse, Response response) {
            List<onDemandPCA> pcas = contactResponse.pcas;
            globals.workers = pcas;
            if (pcas.size() > 0) {
                setWorkers(pcas);
            }
            else {
                setNoWorkers();
            }
            dialog.dismiss();
        }

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
                dialog.dismiss();
            }
            else {
                dialog.dismiss();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Request Failed", sdkVersion).show();
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SelectActivity.class));
        finish();
    }


    private void setLoadingDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        //noinspection ConstantConditions
        dialog.getWindow().setBackgroundDrawableResource(R.color.clear);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
