package com.homecaretimesheets.ondemandpca;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MyShiftsActivity extends AppCompatActivity {
    Context context;
    private int sdkVersion;
    private ApiService apiService;
    private User user;
    OnDemandGlobal globals;
    GridView shiftView;
    OnDemandStorage store;
    Dialog dialog;
    LinearLayout pcaNoShifts;
    LinearLayout recipNoShifts;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy", Locale.US);
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm aa", Locale.US);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = MyShiftsActivity.this;
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_my_shifts);
        globals = (OnDemandGlobal) getApplicationContext();
        store = new OnDemandStorage(context);
        user = globals.user;
        apiService = globals.apiService;
        LinearLayout top = (LinearLayout) findViewById(R.id.my_shift_top);
        shiftView = (GridView) findViewById(R.id.my_grid_layout);
        globals.back_to_find = false;
        globals.currentActivity = 7;
        pcaNoShifts = (LinearLayout) findViewById(R.id.no_shifts_pca);
        pcaNoShifts.setVisibility(View.GONE);
        Button findShifts = (Button) findViewById(R.id.find_shifts_button);
        findShifts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToFindShifts();
            }
        });
        recipNoShifts = (LinearLayout) findViewById(R.id.no_shifts_recip);
        recipNoShifts.setVisibility(View.GONE);
        Button makeShift = (Button) findViewById(R.id.make_shift_button);
        makeShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMakeShift();
            }
        });
        if (user.isPca) {
            @SuppressLint("InflateParams")
            View child = getLayoutInflater().inflate(R.layout.pca_shift_top, null);
            top.addView(child);
            setProfileImage();
            top.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToFindShifts();
                }
            });
            setLoadingDialog();
            getShiftData();
        } else {
            top.setVisibility(View.GONE);
            setLoadingDialog();
            getRecipShifts();
        }
        globals.freshVariable.setListener(new RefreshVariable.ChangeListener() {
            @Override
            public void onChange() {
                setLoadingDialog();
                if (user.isPca) {
                    getShiftData();
                }
                else {
                    getRecipShifts();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (globals.user.isPca) {
            getMenuInflater().inflate(R.menu.pca, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.recipient, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.prof_bar) {
            goToProfile();
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.logout_bar) {
            globals.resetLogin(context);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToProfile() {
        startActivity(new Intent(context,ProfileActivity.class));
        finish();
    }

    private void setLoadingDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        //noinspection ConstantConditions
        dialog.getWindow().setBackgroundDrawableResource(R.color.clear);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        recipNoShifts.setVisibility(View.GONE);
        pcaNoShifts.setVisibility(View.GONE);
    }

    private void getShiftData() {
        ApiService.GetPCAShifts getPCAShifts = apiService.restAdapter.create(ApiService.GetPCAShifts.class);
        getPCAShifts.getShifts(globals.myPCA.id, shiftResp);
    }

    private void getRecipShifts() {
        ApiService.GetRecipShifts getRecipShifts = apiService.restAdapter.create(ApiService.GetRecipShifts.class);
        getRecipShifts.getShifts(globals.recipient.id, shiftResp);
    }

    protected Callback<List<Shift>> shiftResp = new Callback<List<Shift>>() {

        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
                dialog.dismiss();
                globals.resetLogin(context);
                finish();
            }
            else {
                dialog.dismiss();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    globals.resetLogin(context);
                    finish();
                }
                else {
                    Toast.makeText(context, "Shift Request Failed", sdkVersion).show();
                }
            }
        }
        @Override
        public void success(List<Shift> shiftList, Response response) {
            List<Shift> shifts = convertAddress(shiftList);
            if (shifts.size() > 0) {
                setShifts(shifts);
            }
            else {
                setNoShifts();
            }
            dialog.dismiss();
        }
    };

    public void setNoShifts() {
        if (user.isPca) {
            pcaNoShifts.setVisibility(View.VISIBLE);
        }
        else {
            recipNoShifts.setVisibility(View.VISIBLE);
        }
    }

    public List<Shift> convertAddress(List<Shift> shifts) {
        Geocoder geoCoder = new Geocoder(context);
        List<Shift> newList = new ArrayList<>();
        for (Shift shift : shifts) {
            boolean isAvailable = true;
            for (RSVP rsvp : shift.rsvps) {
                if (isAvailable) {
                    if (rsvp.accepted) {
                        isAvailable = false;
                    }
                }
            }
            shift.available = isAvailable;
            boolean addMe = false;
            try {
                Date start = serverDateFormat.parse(shift.startShiftTime);
                if (start.getTime() > globals.getStartOfDay().getTime()) {
                    addMe = true;
                    Date end = serverDateFormat.parse(shift.endShiftTime);
                    shift.timeString = dateFormat.format(start) + " " + timeFormat.format(start) + " - " + timeFormat.format(end);
                }
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            if (addMe) {
                if (shift.address != null && !shift.address.isEmpty() && user.isPca) {
                    try {
                        List<android.location.Address> addressList = geoCoder.getFromLocationName(shift.address, 1);
                        if (addressList.size() > 0) {
                            shift.lat = addressList.get(0).getLatitude();
                            shift.lon = addressList.get(0).getLongitude();
                            shift.distance = setDistance(shift);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                newList.add(shift);
            }
        }
        return newList;
    }

    public String setDistance(Shift shift) {
        String returnString  = "??mi away";
        if (shift.lat != 0 || shift.lon != 0 || globals.currentLocation == null) {
            Location newSpot = new Location(globals.currentLocation);
            newSpot.setLatitude(shift.lat);
            newSpot.setLongitude(shift.lon);
            Float distance = globals.currentLocation.distanceTo(newSpot);
            double miles = distance * 0.00062137;
            returnString = (Double.valueOf((double) Math.round(miles * 100d) / 100d)).toString() + "mi away";
        }
        return returnString;
    }

    public void setShifts(final List<Shift> shifts) {
        globals.shifts = shifts;
        ShiftsGridViewAdapter adapter = new ShiftsGridViewAdapter(context, shifts, user.isPca);
        shiftView.setAdapter(adapter);
        shiftView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getDetail(shifts.get(position));
            }
        });
        dialog.dismiss();
    }

    public void getDetail(Shift shift) {
        globals.currentShift = shift;
        if (user.isPca) {
            startActivity(new Intent(context, ShiftDetailActivity.class));
        }
        else {
            startActivity(new Intent(context, RecipShiftDetailActivity.class));
        }
        finish();
    }

    private void setProfileImage() {
        if (globals.profileImg != null){
            ImageView profileView = (ImageView) findViewById(R.id.profileImage);
            profileView.setImageURI(Uri.fromFile(globals.profileImg));
        }
    }

    public void returnToSelect() {
        startActivity(new Intent(context, SelectActivity.class));
        finish();
    }

    public void goToMakeShift() {
        startActivity(new Intent(context, NewShiftActivity.class));
        finish();
    }

    public void goToFindShifts() {
        startActivity(new Intent(context, FindShiftsActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        returnToSelect();
    }
}
