package com.homecaretimesheets.ondemandpca;

import android.graphics.Bitmap;

import java.io.File;
import java.util.List;


@SuppressWarnings("unused")
class Agency {
    public Integer id;
    public String agency_name;
    public String phone_number;
    public State state;
    String image_name;
    public List<Service> services;
}

class UserData {
    public Integer id;
    String email;
    String userType;
}

@SuppressWarnings("unused")
class Recipient {
    public Integer id;
    String first_name;
    String last_name;
    String home_address;
    public String ma_number;
    public boolean skip_verification;
    public boolean skip_gps;
}

@SuppressWarnings("unused")
class PCA {
    public Integer id;
    public String first_name;
    public String last_name;
    public String umpi;
    public String company_assigned_id;
    public String profilePhoto;
    public String home_address;
    public Integer years_of_experience;
    public Integer gender;
    public List<Service> services;
    public List<Qualification> qualifications;
    public String about_me;
    public String phoneNumber;
    public String email;
    public Boolean available;
    public List<Shift> direct_requests;

}

@SuppressWarnings("unused")
class Shift {
    int id;
    int genderPreference;
    String startShiftTime;
    String endShiftTime;
    List<RSVP> rsvps;
    String address;
    Recipient recipient;
    String additional_requirements;
    double lat;
    double lon;
    String distance = "??mi away";
    String timeString = "--/--/-- --:-- - --:--";
    boolean mine = false;
    boolean available = true;
    boolean direct = false;
    Integer requested_id;
    onDemandPCA pca;
}

@SuppressWarnings("unused")
class RSVP {
    int id;
    String eta;
    boolean accepted;
    boolean declined;
    String cancellationReason;
    onDemandPCA pca;
}

class County {
    int id;
    String name;
}

@SuppressWarnings("unused")
class onDemandPCA {
    public Integer id;
    String first_name;
    String last_name;
    String name;
    public String phoneNumber;
    public String email;
    public String umpi;
    public String company_assigned_id;
    public Image image;
    public String home_address;
    public String about_me;
    Integer gender;
    public List<Service> services;
    public List<Qualification> qualifications;
    Boolean available = false;
    Integer years_of_experience;
    List<County> counties;
    String profilePhoto;
    public Bitmap profileImage;
    public List<Shift> direct_requests;

    public String getName() {
        name = "";
        if (first_name != null) {
            name += first_name;
        }
        if (last_name != null) {
            name += " " + last_name;
        }
        return name;
    }
}

@SuppressWarnings("unused")
class Image {
    public Integer id;
    public String path;
    public String original_name;
    public String mime_type;
    public File file;
}

class Qualification {
    public int id;
    String qualification;
}

@SuppressWarnings("unused")
class State {
    public Integer id;
    public String state_name;
}

@SuppressWarnings("unused")
class Service {
    public Integer id;
    public String service_name;
    public Integer service_number;
}

@SuppressWarnings("unused")
class ContactInfo {
    public Integer id;
    public String name;
    public String phoneNumber;
    public String email;
}